module Assets
{
	export class AssetLoader
	{
		base: string;
		errors: number;
		files: { [url: string]: any };
		numFinished: number;
		numStarted: number;
		ondone: () => void;
		onerror: (url: string) => void;
		constructor(base: string = '')
		{
			this.base = base;
			this.errors = 0;
			this.files = {};
			this.numFinished = 0;
			this.numStarted = 0;
			this.ondone = () => {};
			this.onerror = (url) => {};
		}
		load(url: string, callback: (url: string, response: any) => boolean, binary = false)
		{
			if (this.files[url.toLowerCase()])
			{
				callback(url, this.files[url.toLowerCase()]);
				return;
			}
			
			var request: XMLHttpRequest = new XMLHttpRequest();
			request.onreadystatechange = () => {
				if (request.readyState != 4)
					return;
				
				this.numFinished += 1;
				
				if (request.status != 200)
				{
					this.errors += 1;
					this.onerror(url);
					this._ondone();
					return;
				}
				
				if (callback(url, request.response))
				{
					this.files[url.toLowerCase()] = request.response;
				}
				this._ondone();
			};
			this.numStarted += 1;
			request.open("GET", this.base + "/" + url, true);
			if (binary)
				request.responseType = 'arraybuffer';
			request.send();
		}
		private _ondone()
		{
			if (this.numFinished == this.numStarted)
			{
				this.ondone();
				this.ondone = () => {};
			}
		}
		wait(callback: () => void)
		{
			if (this.numFinished == this.numStarted)
			{
				callback();
			}
			else
			{
				this.ondone = callback;
			}
		}
	}
}