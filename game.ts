/// <reference path="assets.ts" />
/// <reference path="client.ts" />
/// <reference path="compression.ts" />
/// <reference path="pixi.d.ts" />
/// <reference path="underscore.d.ts" />
/// <reference path="visibility.ts" />

module Game
{
	export var theChat: Chat;
	export var theClient: Client;
	export var theAssets: Assets.AssetLoader;
	export var thePlayer: Player;
	export var theStrings: JStrings;
	export var theThings: ThingDb;
	export var theWorld: World;
	
	var theGradient: CanvasGradient;
	
	var imagesCache: { [id: number]: PIXI.Texture };
	var imagesIdx: Uint32Array[];
	var imagesPkg: Uint8Array[];
	
	export function connect(url: string)
	{
		thePlayer = null;
		theWorld = new World();
		theClient = new Client(url);
		
		window.addEventListener('beforeunload', (ev) => {
			theClient.sendquit();
		});
	}
	
	export function main(url: string, renderer: PIXI.CanvasRenderer | PIXI.WebGLRenderer, chatDiv: HTMLDivElement, chatInput: HTMLInputElement)
	{
		imagesCache = {};
		theAssets = new Assets.AssetLoader("assets");
		theAssets.onerror = (url: string) => {
			console.log("Error loading " + url);
		};
		theAssets.load("Estate.json", (url, response) => true);
		theAssets.load("Strings.json", (url, response) => {
			theStrings = JSON.parse(response);
			return false;
		});
		theAssets.load("ThingDb.json", (url, response) => {
			var thingdb: JThingDb = JSON.parse(response);
			theThings = new ThingDb(thingdb);
			return false;
		});
		imagesIdx = [];
		imagesPkg = [];
		for (var i = 0; i < 140; i++)
		{
			((i) => {
				theAssets.load("images/"+i.toString()+".idx3", (url, response) => {
					var blob: ArrayBuffer = response;
					imagesIdx[i] = new Uint32Array(blob);
					return false;
				}, true);
				theAssets.load("images/"+i.toString()+".pkg3", (url, response) => {
					var blob: ArrayBuffer = response;
					imagesPkg[i] = new Uint8Array(blob);
					return false;
				}, true);
			})(i);
		}
		theAssets.wait(() => {
			if (!theAssets.errors)
			{
				theChat = new Chat(chatDiv, chatInput);
				connect(url);
				theWorld.render(renderer);
			}
		});
	}
	
	function createobject(extent: number, type: number) : Object
	{
		var thing = theThings.objects[type];
		switch (thing.update)
		{
		case "HarpoonUpdate":
			return new HarpoonObject(extent, type);
		case "PlayerUpdate":
			return new PlayerObject(extent, type);
		case "ProjectileUpdate":
			return new ProjectileObject(extent, type);
		default:
			return new Object(extent, type);
		}
	}
	
	function drawimage(imageData: ImageData, id: number)
	{
		var destWidth = imageData.width,
			destHeight = imageData.height,
			destData = imageData.data;
		
		var subdir = Math.floor(id / 1000);
		var offset = imagesIdx[subdir][(id % 1000) * 3];
		var length = imagesIdx[subdir][(id % 1000) * 3 + 1] & 0x7fffffff;
		var coords = imagesIdx[subdir][(id % 1000) * 3 + 2];
		
		var realWidth = coords & 0xffff;
		var realHeight = coords >> 16;
		var uncomp: Uint8Array = Compression.decompress(imagesPkg[subdir].subarray(offset, offset + length));
		var compWidth = uncomp[0];
		var compHeight = uncomp[1];
		var startX = realWidth - compWidth;
		var startY = realHeight - compHeight;
		
		var uncompOffset = 2;
		for (var y = startY; y < realHeight; ++y)
		{
			for (var x = startX; x < realWidth; ++x)
			{
				var r, g, b, a;
				r = uncomp[uncompOffset++];
				g = uncomp[uncompOffset++];
				b = uncomp[uncompOffset++];
				a = uncomp[uncompOffset++];
				
				if (a != 0)
				{
					destData[(destWidth * y + x) * 4 + 0] = r;
					destData[(destWidth * y + x) * 4 + 1] = g;
					destData[(destWidth * y + x) * 4 + 2] = b;
					destData[(destWidth * y + x) * 4 + 3] = a;
				}
			}
		}
	}
	
	function getimage(id: number, oncomplete?: (id: number) => void) : PIXI.Texture
	{
		if (imagesCache[id])
			return imagesCache[id];
		var subdir = Math.floor(id / 1000);
		var offset = imagesIdx[subdir][(id % 1000) * 3];
		var length = imagesIdx[subdir][(id % 1000) * 3 + 1];
		var coords = imagesIdx[subdir][(id % 1000) * 3 + 2];
		if (length & 0x80000000)
			return PIXI.Texture.EMPTY;
		var realWidth = coords & 0xffff;
		var realHeight = coords >> 16;
		var blob = new Blob([imagesPkg[subdir].subarray(offset, offset + length)], { type: 'image/png' });
		var src = URL.createObjectURL(blob);
		imagesCache[id] = PIXI.Texture.fromImage(src);
		imagesCache[id].baseTexture.addListener('error', () => {
			URL.revokeObjectURL(src);
			if (oncomplete)
				oncomplete(id);
		});
		imagesCache[id].baseTexture.addListener('loaded', () => {
			URL.revokeObjectURL(src);
			if (oncomplete)
				oncomplete(id);
		});
		imagesCache[id].addListener('update', () => {
			var crop = imagesCache[id].crop;
			crop.x = crop.width - realWidth;
			crop.width = realWidth;
			crop.y = crop.height - realHeight;
			crop.height = realHeight;
			imagesCache[id].removeAllListeners('update');
		});
		return imagesCache[id];
	}
	
	function getimagepath(id: number) : string
	{
		var subdir = Math.floor(id / 1000).toString(10);
		return theAssets.base + "/images/" + subdir + "/" + id.toString(10) + ".png"; 
	}
	
	export enum Action
	{
		Move = 0x01,
		Jump = 0x02,
		Attack = 0x04
	}
	
	export enum PlayerClass
	{
		WARRIOR,
		WIZARD,
		CONJURER
	}
	
	export enum ThingFlags
	{
		AIRBORNE = 0x1,
		ALLOW_OVERLAP = 0x2,
		BELOW = 0x4,
		DANGEROUS = 0x8,
		EDIT_VISIBLE = 0x10,
		FLICKER = 0x20,
		IMMOBILE = 0x40,
		MISSILE_HIT = 0x80,
		NO_AUTO_DROP = 0x100,
		NO_COLLIDE = 0x200,
		NO_COLLIDE_OWNER = 0x400,
		NO_PUSH_CHARACTERS = 0x800,
		NONE = 0x1000,
		OWNER_VISIBLE = 0x2000,
		RESPAWN = 0x4000,
		SHADOW = 0x8000,
		SHORT = 0x10000,
		SIGHT_DESTROY = 0x20000,
	}
	
	export enum ThingClass
	{
		MISSILE = 0x000001,
		MONSTER = 0x000002,
		PLAYER = 0x000004,
		OBSTACLE = 0x000008,
		FOOD = 0x000010,
		EXIT = 0x000020,
		KEY = 0x000040,
		DOOR = 0x000080,
		INFO_BOOK = 0x000100,
		TRIGGER = 0x000200,
		TRANSPORTER = 0x000400,
		HOLE = 0x000800,
		WAND = 0x001000,
		FIRE = 0x002000,
		ELEVATOR = 0x004000,
		ELEVATOR_SHAFT = 0x008000,
		DANGEROUS = 0x010000,
		MONSTERGENERATOR = 0x020000,
		READABLE = 0x040000,
		LIGHT = 0x080000,
		SIMPLE = 0x100000,
		COMPLEX = 0x200000,
		IMMOBILE = 0x400000,
		VISIBLE_ENABLE = 0x800000,
		WEAPON = 0x1000000,
		ARMOR = 0x2000000,
		NOT_STACKABLE = 0x4000000,
		TREASURE = 0x8000000,
		FLAG = 0x10000000,
		CLIENT_PERSIST = 0x20000000,
		CLIENT_PREDICT = 0x40000000,
		PICKUP = 0x80000000,
	}
	
	export enum WallFacing
	{
		UP,
		DOWN,
		CROSS,
		TNORTHWEST,
		TNORTHEAST,
		TSOUTHEAST,
		TSOUTHWEST,
		ARROWDOWN,
		ARROWLEFT,
		ARROWUP,
		ARROWRIGHT,
		WINDOWUP,
		WINDOWDOWN,
		RIGHTHALFARROWUP,
		LETHALFARROWUP		
	}
	
	export enum Enchant
	{
		ENCHANT_INVISIBLE,
		ENCHANT_MOONGLOW,
		ENCHANT_BLINDED,
		ENCHANT_CONFUSED,
		ENCHANT_SLOWED,
		ENCHANT_HELD,
		ENCHANT_DETECTING,
		ENCHANT_ETHEREAL,
		ENCHANT_RUN,
		ENCHANT_HASTED,
		ENCHANT_VILLAIN,
		ENCHANT_AFRAID,
		ENCHANT_BURNING,
		ENCHANT_VAMPIRISM,
		ENCHANT_ANCHORED,
		ENCHANT_LIGHT,
		ENCHANT_DEATH,
		ENCHANT_PROTECT_FROM_FIRE,
		ENCHANT_PROTECT_FROM_POISON,
		ENCHANT_PROTECT_FROM_MAGIC,
		ENCHANT_PROTECT_FROM_ELECTRICITY,
		ENCHANT_INFRAVISION,
		ENCHANT_SHOCK,
		ENCHANT_INVULNERABLE,
		ENCHANT_TELEKINESIS,
		ENCHANT_FREEZE,
		ENCHANT_SHIELD,
		ENCHANT_REFLECTIVE_SHIELD,
		ENCHANT_CHARMING,
		ENCHANT_ANTI_MAGIC,
		ENCHANT_CROWN,
		ENCHANT_SNEAK
	}
	
	export enum PlayerArmor
	{
        NO_ARMOR = 0,
        ALL_CLOTH_ARMOR = 0x4C0F,
        ALL_LEATHER_ARMOR = 0x2090D0,
        ALL_TORSO_ARMOR = 0x3CC02,
        ALL_HELM_ARMOR = 0x0FC0000,
        ALL_ARM_ARMOR = 0x3000,
        ALL_FEET_ARMOR = 0x1C1,
        ALL_LEG_ARMOR = 0x23C,
        ALL_HAND_ARMOR = 0x3000000,
        STREET_PANTS = 0x4,
        MEDIEVAL_PANTS = 0x8,
        STREET_SNEAKERS = 0x1,
        LEATHER_BOOTS = 0x40,
        LEATHER_ARMORED_BOOTS = 0x80,
        PLATE_BOOTS = 0x100,
        LEATHER_LEGGINGS = 0x10,
        CHAIN_LEGGINGS = 0x20,
        PLATE_LEGGINGS = 0x200,
        STREET_SHIRT = 0x400,
        MEDIEVAL_SHIRT = 0x800,
        WIZARD_ROBE = 0x4000,
        LEATHER_TUNIC = 0x8000,
        CHAIN_TUNIC = 0x10000,
        PLATE_BREAST = 0x20000,
        LEATHER_ARMBANDS = 0x1000,
        PLATE_ARMS = 0x2000,
        MEDIEVAL_CLOAK = 0x2,
        ROUND_SHIELD = 0x1000000,
        KITE_SHIELD = 0x2000000,
        CHAIN_COIF = 0x40000,
        WIZARD_HELM = 0x80000,
        CONJURER_HELM = 0x100000,
        LEATHER_HELM = 0x200000,
        PLATE_HELM = 0x400000,
        ORNATE_HELM = 0x800000
    }
	
	export enum PlayerStance
    {
        IDLE,
        DIE,
        DEAD,
        JUMP,
        WALK,
        WALK_AND_DRAG,
        RUN,
        RUNNING_JUMP,
        PICKUP,
        DODGE_LEFT,
        DODGE_RIGHT,
        ELECTROCUTED,
        FALL,
        TRIP,
        GET_UP,
        LAUGH,
        POINT,
        SIT,
        SLEEP,
        TALK,
        TAUNT,
        CAST_SPELL,
        CONCENTRATE,
        PUNCH_LEFT,
        PUNCH_RIGHT,
        PUNCH_RIGHT_HOOK,
        MACE_STRIKE,
        SWORD_STRIKE,
        LONG_SWORD_STRIKE,
        STAFF_STRIKE,
        STAFF_BLOCK,
        STAFF_SPELL_BLAST,
        STAFF_THRUST,
        SHOOT_BOW,
        SHOOT_CROSSBOW,
        AXE_STRIKE,
        GREAT_SWORD_PARRY,
        GREAT_SWORD_STRIKE,
        GREAT_SWORD_IDLE,
        HAMMER_STRIKE,
        RAISE_SHIELD,
        RECOIL_FORWARD,
        RECOIL_BACKWARD,
        RECOIL_SHIELD,
        CHAKRAM_STRIKE,
        BERSERKER_CHARGE,
        WARCRY,
        GREAT_SWORD_BLOCK_LEFT,
        GREAT_SWORD_BLOCK_RIGHT,
        GREAT_SWORD_BLOCK_DOWN,
        ELECTRIC_ZAP,
        DUST,
        RECOIL,
        SNEAK,
        HARPOONTHROW
    }
	
	export enum PlayerWeapon
    {
        NO_WEAPONS = 0x0,
        ALL_WEAPONS = 0x7FFFFFE,
        ALL_MAGICAL_STAVES = 0x47F0000,
        ALL_STAVES = 0x7FF8000,
        ALL_RANGED_WEAPONS = 0x47F00FE,
        FLAG = 0x1,
        QUIVER = 0x2,
        BOW = 0x4,
        CROSSBOW = 0x8,
        ARROW = 0x10,
        BOLT = 0x20,
        CHAKRAM = 0x40,
        SHURIKEN = 0x80,
        SWORD = 0x100,
        LONG_SWORD = 0x200,
        GREAT_SWORD = 0x400,
        MACE = 0x800,
        AXE = 0x1000,
        OGRE_AXE = 0x2000,
        HAMMER = 0x4000,
        STAFF = 0x8000,
        STAFF_SULPHOROUS_FLARE = 0x10000,
        STAFF_SULPHOROUS_SHOWER = 0x20000,
        STAFF_LIGHTNING = 0x40000,
        STAFF_FIREBALL = 0x80000,
        STAFF_TRIPLE_FIREBALL = 0x100000,
        STAFF_FORCE_OF_NATURE = 0x200000,
        STAFF_DEATH_RAY = 0x400000,
        STAFF_OBLIVION_HALBERD = 0x800000,
        STAFF_OBLIVION_HEART = 0x1000000,
        STAFF_OBLIVION_WIERDLING = 0x2000000,
        STAFF_OBLIVION_ORB = 0x4000000
    }
	
	export enum PlayerAbility
	{
		ABILITY_BERSERKER_CHARGE = 1,
		ABILITY_WARCRY,
		ABILITY_HARPOON,
		ABILITY_TREAD_LIGHTLY,
		ABILITY_EYE_OF_THE_WOLF
	}
	
	var ModelArmor = {};
	ModelArmor[PlayerArmor.CHAIN_COIF] = 14344;
	ModelArmor[PlayerArmor.CHAIN_LEGGINGS] = 14309;
	ModelArmor[PlayerArmor.CHAIN_TUNIC] = 14343;
	ModelArmor[PlayerArmor.CONJURER_HELM] = 14365;
	ModelArmor[PlayerArmor.KITE_SHIELD] = 14374;
	ModelArmor[PlayerArmor.LEATHER_ARMBANDS] = 14345;
	ModelArmor[PlayerArmor.LEATHER_ARMORED_BOOTS] = 14347;
	ModelArmor[PlayerArmor.LEATHER_BOOTS] = 14346;
	ModelArmor[PlayerArmor.LEATHER_HELM] = 14358;
	ModelArmor[PlayerArmor.LEATHER_LEGGINGS] = 14359;
	ModelArmor[PlayerArmor.LEATHER_TUNIC] = 14357;
	ModelArmor[PlayerArmor.MEDIEVAL_CLOAK] = 14307;
	ModelArmor[PlayerArmor.MEDIEVAL_PANTS] = 14354;
	ModelArmor[PlayerArmor.MEDIEVAL_SHIRT] = 14353;
	ModelArmor[PlayerArmor.ORNATE_HELM] = 14364;
	ModelArmor[PlayerArmor.PLATE_ARMS] = 14362;
	ModelArmor[PlayerArmor.PLATE_BOOTS] = 14356;
	ModelArmor[PlayerArmor.PLATE_BREAST] = 14360;
	ModelArmor[PlayerArmor.PLATE_HELM] = 14361;
	ModelArmor[PlayerArmor.PLATE_LEGGINGS] = 14363;
	ModelArmor[PlayerArmor.ROUND_SHIELD] = 14373;
	ModelArmor[PlayerArmor.STREET_PANTS] = 14351;
	ModelArmor[PlayerArmor.STREET_SHIRT] = 14352;
	ModelArmor[PlayerArmor.STREET_SNEAKERS] = 14350;
	ModelArmor[PlayerArmor.WIZARD_HELM] = 14372;
	ModelArmor[PlayerArmor.WIZARD_ROBE] = 14371;
	
	var ModelWeapon = {};
	ModelWeapon[PlayerWeapon.FLAG] = 0;
	ModelWeapon[PlayerWeapon.QUIVER] = 14348;
	ModelWeapon[PlayerWeapon.BOW] = 14381;
	ModelWeapon[PlayerWeapon.CROSSBOW] = 14366;
	ModelWeapon[PlayerWeapon.ARROW] = 0;
	ModelWeapon[PlayerWeapon.BOLT] = 0;
	ModelWeapon[PlayerWeapon.CHAKRAM] = 14367;
	ModelWeapon[PlayerWeapon.SHURIKEN] = 14368;
	ModelWeapon[PlayerWeapon.SWORD] = 14378;
	ModelWeapon[PlayerWeapon.LONG_SWORD] = 14375;
	ModelWeapon[PlayerWeapon.GREAT_SWORD] = 14379;
	ModelWeapon[PlayerWeapon.MACE] = 14377;
	ModelWeapon[PlayerWeapon.AXE] = 14380;
	ModelWeapon[PlayerWeapon.OGRE_AXE] = 14398;
	ModelWeapon[PlayerWeapon.HAMMER] = 14376;
	ModelWeapon[PlayerWeapon.STAFF] = 14369;
	ModelWeapon[PlayerWeapon.STAFF_SULPHOROUS_FLARE] = 14383;
	ModelWeapon[PlayerWeapon.STAFF_SULPHOROUS_SHOWER] = 14384;
	ModelWeapon[PlayerWeapon.STAFF_LIGHTNING] = 14385;
	ModelWeapon[PlayerWeapon.STAFF_FIREBALL] = 14386;
	ModelWeapon[PlayerWeapon.STAFF_TRIPLE_FIREBALL] = 14387;
	ModelWeapon[PlayerWeapon.STAFF_FORCE_OF_NATURE] = 14388;
	ModelWeapon[PlayerWeapon.STAFF_DEATH_RAY] = 14391;
	ModelWeapon[PlayerWeapon.STAFF_OBLIVION_HALBERD] = 14392;
	ModelWeapon[PlayerWeapon.STAFF_OBLIVION_HEART] = 14393;
	ModelWeapon[PlayerWeapon.STAFF_OBLIVION_WIERDLING] = 14394;
	ModelWeapon[PlayerWeapon.STAFF_OBLIVION_ORB] = 14395;
	
	// Stock game data
	var AbilityDelays = {};
	AbilityDelays[PlayerAbility.ABILITY_BERSERKER_CHARGE] = [300];
	AbilityDelays[PlayerAbility.ABILITY_WARCRY] = [300];
	AbilityDelays[PlayerAbility.ABILITY_HARPOON] = [150];
	AbilityDelays[PlayerAbility.ABILITY_TREAD_LIGHTLY] = [30];
	AbilityDelays[PlayerAbility.ABILITY_EYE_OF_THE_WOLF] = [600];
	
	var AbilityImages = {};
	AbilityImages[PlayerAbility.ABILITY_BERSERKER_CHARGE] = [132051, 132056];
	AbilityImages[PlayerAbility.ABILITY_WARCRY] = [132049, 132054];
	AbilityImages[PlayerAbility.ABILITY_HARPOON] = [135678, 135679];
	AbilityImages[PlayerAbility.ABILITY_TREAD_LIGHTLY] = [132048, 132053];
	AbilityImages[PlayerAbility.ABILITY_EYE_OF_THE_WOLF] = [132052, 132057];
	
	var EnchantImages = {};
	EnchantImages[Enchant.ENCHANT_INFRAVISION] = 131875;
	
	var QuickBarSlots = [
		PlayerAbility.ABILITY_BERSERKER_CHARGE,
		PlayerAbility.ABILITY_WARCRY,
		PlayerAbility.ABILITY_HARPOON,
		PlayerAbility.ABILITY_TREAD_LIGHTLY,
		PlayerAbility.ABILITY_EYE_OF_THE_WOLF
	];
	
	var HealthPotions = [
		/* RedPotion */ 636,
		/* Meat */ 645,
		/* RedApple*/ 649
	];
	
	interface JMap
	{
		type: string;
		objects: JMapObject[];
		tiles: JMapTile[];
		walls: JMapWall[];
	}
	
	interface JMapObject
	{
		extent: number;
		type: number;
		x: number;
		y: number;
	}
	
	interface JMapTile
	{
		x: number;
		y: number;
		id: number;
		variation: number;
	}
	
	interface JMapWall
	{
		x: number;
		y: number;
		facing: number;
		id: number;
		variation: number;
		destructable: boolean;
		secret: boolean;
		secretId: number;
		window: boolean;
	}
	
	interface JThingDb
	{
		things: JThing[];
		tiles: JTile[];
		walls: JWall[];
	}
	
	interface JTile
	{
		id: number;
		rows: number;
		cols: number;
		frames: number;
		images: number[];
	}
	
	interface JPlayerImage
	{
		name: string;
		delay: number;
		type: string;
		naked: number[];
		armor: { [id: number]: number[] };
		weapon: { [id: number]: number[] };
	}
	
	interface JThing
	{
		id: number;
		name: string;
		prettyname: string;
		x: number;
		y: number;
		z: number;
		height: number;
		width: number;
		cls: number;
		flags: number;
		update: string;
		draw: string;
		images: number[];
		frameTimer: number;
		playerImages?: { [stance: number]: JPlayerImage };
	}
	
	interface JWall
	{
		id: number;
		name: string;
		images: {x: number, y: number, image: number}[][][];
	}
	
	interface JStrings
	{
		messages: {[id: string] : string};
	}
	
	export interface IDrawable
	{
		x: number;
		y: number;
		sprite: PIXI.DisplayObject;
		visible: boolean;
		angle: number;
		d: number;
		draw();
	}
	
	var _cachedSparkTextures: {[color: string]: PIXI.Texture} = {};
	function getSparkTexture(color: string)
	{
		if (_cachedSparkTextures[color])
			return _cachedSparkTextures[color];
		
		var buf = new PIXI.CanvasBuffer(20, 20);
		var grad = buf.context.createRadialGradient(10,10,10,10,10,0);
		grad.addColorStop(0, 'transparent');
		grad.addColorStop(1, color);
		buf.context.fillStyle = grad;
		buf.context.fillRect(0, 0, 20, 20);
		buf.context.fillStyle = 'white';
		buf.context.beginPath();
		buf.context.arc(10, 10, 1, 0, 2 * Math.PI);
		buf.context.fill();
		_cachedSparkTextures[color] = PIXI.Texture.fromCanvas(buf.canvas);
		return _cachedSparkTextures[color];
	}
	
	export class SparkDrawable implements IDrawable
	{
		x: number;
		y: number;
		z: number;
		dx: number;
		dy: number;
		dz: number;
		sprite: PIXI.Sprite;
		visible: boolean;
		angle: number;
		d: number;
		frame: number;
		constructor(color: string, x?: number, y?: number)
		{
			this.sprite = new PIXI.Sprite(getSparkTexture(color));
			this.sprite.blendMode = PIXI.BLEND_MODES.ADD;
			this.sprite.anchor = new PIXI.Point(0.5,0.5);
			this.visible = true;
			this.frame = 0;
			this.x = x;
			this.y = y;
            this.z = Math.random() * 20;
            this.dz = 0;
            this.dx = (Math.random() * 2 - 1) * 0.5;
            this.dy = (Math.random() * 2 - 1) * 0.5;
		}
		draw()
		{
            this.dz -= 0.9;
            this.z += this.dz;
            if (this.z <= 0)
                this.dz = 5;
            this.frame++;
            this.x += this.dx;
            this.y += this.dy;
            this.sprite.x = Math.floor(this.x);
            this.sprite.y = Math.floor(this.y - this.z - 20);
            if (this.frame >= 10)
                this.sprite.alpha = (10 - (this.frame-10)) / 20;
            if (this.frame >= 20)
                this.visible = false;
		}
	}
	
	export class Chat
	{
		chatDiv: HTMLDivElement;
		chatInput: HTMLInputElement;
		constructor(chatDiv: HTMLDivElement, chatInput: HTMLInputElement)
		{
			this.chatDiv = chatDiv;
			this.chatInput = chatInput;
		
		}
		hasFocus(): boolean
		{
			return document.activeElement == this.chatInput;
		}
		loseFocus()
		{
			this.chatInput.blur();
		}
		takeFocus()
		{
			this.chatInput.focus();
		}
		clearInput()
		{
			this.chatInput.value = '';
		}
		getInput()
		{
			return this.chatInput.value;
		}
		trimLog()
		{
			while (this.chatDiv.children.length >= 50)
			{
				this.chatDiv.removeChild(this.chatDiv.firstChild);
			}		
		}
		addChat(name: string, msg: string, me=false)
		{
			var el = document.createElement('div');
			el.innerHTML = '<div class="chatmsg">&lt;<span class="chatname '+(me?'chatme':'')+'">'+_.escape(name)+'</span>&gt; <span class="chattext">'+_.escape(msg)+'</span></div>';
			this.chatDiv.appendChild(el);
			this.trimLog();
		}
		addMessage(msg: string)
		{
			var el = document.createElement('div');
			el.innerHTML = '<div class="chatsystemmsg">* '+_.escape(msg)+'</div>';
			this.chatDiv.appendChild(el);
			this.trimLog();
		}
		addKill(victim: string, attacker: string, mevictim=false, meattacker=false)
		{
			var el = document.createElement('div');
			el.innerHTML = '<div class="chatkill"><span class="chatname '+(mevictim?'chatme':'')+'">'+_.escape(victim)+'</span> was killed by <span class="chatname '+(meattacker?'chatme':'')+'">'+_.escape(attacker)+'</span></div>';
			this.chatDiv.appendChild(el);
			this.trimLog();
		}
	}
	
	export class Container extends PIXI.DisplayObject
	{
		children: IDrawable[];
		constructor()
		{
			super();
			
			this.children = [];
		}
		addChild(child: IDrawable)
		{
			var idx = _.sortedIndex(this.children, child, (x) => x.y);
			this.children.splice(idx, 0, child);
			
			child.sprite.parent = <any>this;
		}
		removeChild(child: IDrawable)
		{
			var idx = this.children.indexOf(child);
			this.children.splice(idx, 1);
			
			//child.sprite.parent = null;
		}
		updateChild(child: IDrawable)
		{
			if (child.sprite.parent !== <any>this)
				return;
			this.removeChild(child);
			this.addChild(child);
		}
		updateTransform()
		{
			if (!this.visible)
				return;
			
			this.displayObjectUpdateTransform();
			for (var i = 0, j = this.children.length; i < j; ++i)
				this.children[i].sprite.updateTransform();
		}
		containerUpdateTransform()
		{
			this.updateTransform(); 
		}
		getBounds(matrix?: PIXI.Matrix) : PIXI.Rectangle
		{
			if(!this._currentBounds)
		    {
		
		        if (this.children.length === 0)
		        {
		            return PIXI.Rectangle.EMPTY;
		        }
		
		        // TODO the bounds have already been calculated this render session so return what we have
		
		        var minX = Infinity;
		        var minY = Infinity;
		
		        var maxX = -Infinity;
		        var maxY = -Infinity;
		
		        var childBounds;
		        var childMaxX;
		        var childMaxY;
		
		        var childVisible = false;
		
		        for (var i = 0, j = this.children.length; i < j; ++i)
		        {
		            var child = this.children[i];
		
		            if (!child.visible || !child.sprite.visible)
		            {
		                continue;
		            }
		
		            childVisible = true;
		
		            childBounds = this.children[i].sprite.getBounds();
		
		            minX = minX < childBounds.x ? minX : childBounds.x;
		            minY = minY < childBounds.y ? minY : childBounds.y;
		
		            childMaxX = childBounds.width + childBounds.x;
		            childMaxY = childBounds.height + childBounds.y;
		
		            maxX = maxX > childMaxX ? maxX : childMaxX;
		            maxY = maxY > childMaxY ? maxY : childMaxY;
		        }
		
		        if (!childVisible)
		        {
		            return PIXI.Rectangle.EMPTY;
		        }
		
		        var bounds = this._bounds;
		
		        bounds.x = minX;
		        bounds.y = minY;
		        bounds.width = maxX - minX;
		        bounds.height = maxY - minY;
		
		        this._currentBounds = bounds;
		    }
		
		    return this._currentBounds;
		}
		getLocalBounds() : PIXI.Rectangle
		{
		    var matrixCache = this.worldTransform;
		
		    this.worldTransform = PIXI.Matrix.IDENTITY;
		
		    for (var i = 0, j = this.children.length; i < j; ++i)
		    {
		        this.children[i].sprite.updateTransform();
		    }
		
		    this.worldTransform = matrixCache;
		
		    this._currentBounds = null;
		
		    return this.getBounds( PIXI.Matrix.IDENTITY );
		}
		_renderWebGL(renderer: PIXI.WebGLRenderer)
		{
		}
		renderWebGL(renderer: PIXI.WebGLRenderer)
		{
			if (!this.visible)
				return;
			this._renderWebGL(renderer);
			for (var i = 0, j = this.children.length; i < j; ++i)
			{
				this.children[i].draw();
				this.children[i].sprite.renderWebGL(renderer);
			}
		}
		renderCanvas(renderer: PIXI.CanvasRenderer)
		{
			if (!this.visible)
				return;
			for (var i = 0, j = this.children.length; i < j; ++i)
			{
				if (this.children[i].visible)
				{
					this.children[i].draw();
					this.children[i].sprite.renderCanvas(renderer);
				}
			}
		}
		destroy(destroyChildren: boolean = true)
		{
			super.destroy();
			if (destroyChildren)
			{
				for (var i = 0, j = this.children.length; i < j; ++i)
					this.children[i].sprite.destroy();
			}
			for (var i = 0, j = this.children.length; i < j; ++i)
				this.children[i].sprite.parent = null;
			this.children = null;
		}
	}
	
	export class Effect
	{
		// return false if it should be removed
		draw(graphics: PIXI.Graphics): boolean
		{
			return true;
		}
	}
	
	export class ParticleEffect
	{
		color: number;
		duration: number;
		totalDuration: number;
		radius: number;
		x: number;
		y: number;
		constructor(color: number, x: number, y: number, duration: number, radius?: number)
		{
			this.color = color;
			this.totalDuration = duration;
			this.duration = duration;
			if (radius === null)
				this.radius = 3;
			else
				this.radius = radius;
			this.x = x;
			this.y = y;
		}
		draw(graphics: PIXI.Graphics): boolean
		{
			if (this.duration <= 0)
				return false;
			graphics.beginFill(this.color, this.duration / this.totalDuration)
				.drawCircle(this.x, this.y, this.radius)
				.endFill();
			this.duration--;
			return true;
		}
	}
	
	export class DurationEffect
	{
		type: number;
		extent1: number;
		extent2: number;
		constructor(type: number, extent1: number, extent2: number)
		{
			this.type = type;
			this.extent1 = extent1;
			this.extent2 = extent2;
		}
		draw(graphics: PIXI.Graphics): boolean
		{
			var o1 = theWorld.get(this.extent1),
				o2 = theWorld.get(this.extent2);
			
			if (!o1 || !o2)
				return false;

			switch (this.type)
			{
			case 7:
				graphics.lineColor = 0x8B4513;
				graphics.lineWidth = 3;
				graphics.moveTo(o1.x, o1.y - 30);
				graphics.lineTo(o2.x, o2.y);
				graphics.lineWidth = 0;
				break;
			}
			
			return true;
		}
	}
	
	export class Client
	{
		pendingActions: number;
		playerExtent: number;
		prevMouse: {x: number, y:number};
		prevWeapon: number;
		maxHealth: number;
		currentHealth: number;
		wsclient: WSClient.WSClient;
		constructor(url: string)
		{
			this.prevMouse = {x: 0, y: 0};
			this.prevWeapon = null;
			this.wsclient = new WSClient.WSClient(url, this, window.prompt('Player name?', 'Jack'));
		}
		onchat(extent: number, msg: string, x: number, y: number)
		{
			var name: string;
			var obj: Object = theWorld.get(extent);
			if (obj && obj instanceof PlayerObject)
				name = obj.player.name;
			else
				name = extent.toString(10);
			if (obj == thePlayer.object)
				theChat.addChat(name, msg, true);
			else
				theChat.addChat(name, msg);
		}
		onjoindata(extent: number)
		{
			console.log("Joined server as " + extent.toString(10));
			this.playerExtent = extent;
			theWorld.getOrCreate(extent, 0x2c9);
		}
		onnewmap(mapname: string)
		{
			mapname = mapname.substring(0, mapname.length - 4);
			console.log("Map: " + mapname);
			theWorld.loadMap(mapname);
		}
		onnewplayer(extent: number, cls: number, name: string, wolname: string, respawnArmor: number, respawnWeapon: number)
		{
			console.log("New player: " + name + " (" + wolname + ") = " + extent.toString(10));
			var plr: Player = new Player(extent, cls, name, wolname, respawnArmor, respawnWeapon);
			theWorld.addPlayer(plr);
			if (extent == this.playerExtent)
				thePlayer = plr;
			else
				theWorld.displayMessage(name + " has joined the game.");
		}
		onplayerquit(extent: number)
		{
			theWorld.displayMessage(theWorld.players[extent].name + " has left the game.");
			delete theWorld.players[extent];
		}
		onplayerdied(extent: number)
		{
			if (thePlayer && extent == thePlayer.object.extent)
			{
				this.prevWeapon = null;
				thePlayer.object.armor = thePlayer.respawnArmor;
				thePlayer.object.weapon = thePlayer.respawnWeapon;
				thePlayer.equipment.splice(0, thePlayer.equipment.length);
				thePlayer.inventory.splice(0, thePlayer.inventory.length);
			}
		}
		onupdate(extent: number, type: number, x: number, y: number, other?: Uint8Array)
		{
			if (extent == 0 || type == 0)
				return;
				
			var obj = theWorld.getOrCreate(extent, type);
			obj.update(x, y, other);
		}
		onobjectdestroy(extent: number)
		{
			var obj = theWorld.objects[extent];
			if (obj && !theWorld.players[extent])
			{
				delete theWorld.objects[extent];
				theWorld.objectsArray.splice(theWorld.objectsArray.indexOf(obj), 1);
				theWorld.stage.removeChild(obj);
			}
		}
		onsetobs(extent: number, mode: boolean)
		{
			(<PlayerObject>theWorld.getOrCreate(extent, 0x2c9)).setobs(mode);
		}
		onobeliskcharge(extent: number, frame: number)
		{
			var o = theWorld.get(extent | 0x8000);
			if (!o) return;
			o.currentFrame = Math.floor((frame / 50.0) * 7); // max charge is 50, there are 8 frames
		}
		ondoorangle(extent: number, frame: number)
		{
			var o = theWorld.get(extent | 0x8000);
			if (!o) return;
			o.currentFrame = frame;
		}
		oncharges(extent: number, _current: number, _total: number)
		{
			var o = theWorld.get(extent);
			o.charges = {current: _current, total: _total};
			theWorld.updateWeapon();
		}
		onarmor(extent: number, bitmask: number, equip: boolean)
		{
			var o = <PlayerObject>theWorld.getOrCreate(extent & 0x7fff, 0x2c9);
			o.equiparmor(bitmask, equip);
			if (o == thePlayer.object)
				Game.theWorld.updateInventory();
		}
		onweapon(extent: number, bitmask: number, equip: boolean)
		{
			var o = <PlayerObject>theWorld.getOrCreate(extent & 0x7fff, 0x2c9);
			o.equipweapon(bitmask, equip);
			if (o == thePlayer.object)
				Game.theWorld.updateInventory();
		}
		onequip(extent: number)
		{
			thePlayer.equipment.push(extent);
			Game.theWorld.updateInventory();
			if (theWorld.get(extent).getThing().cls & ThingClass.WEAPON)
				theWorld.updateWeapon();
			if (extent == this.prevWeapon)
				this.prevWeapon = null;
		}
		ondequip(extent: number)
		{
			thePlayer.equipment.splice(thePlayer.equipment.indexOf(extent), 1);
			Game.theWorld.updateInventory();
			
			if (theWorld.get(extent).getThing().cls & ThingClass.WEAPON)
			{
				theWorld.updateWeapon();
			}
		}
		onpickup(extent: number, type: number)
		{
			theWorld.getOrCreate(extent, type);
			console.log(extent, theWorld.get(extent).getThing().name);
			if (theWorld.get(extent).getThing().cls & ThingClass.WEAPON)
				if (this.prevWeapon == null)// || thePlayer.inventory.indexOf(this.prevWeapon) >= 0)
					this.prevWeapon = extent;
			thePlayer.inventory.push(extent);
			Game.theWorld.updateInventory();
		}
		ondrop(extent: number, type: number)
		{
			if (thePlayer.equipment.indexOf(extent) >= 0)
				this.ondequip(extent);
			if (this.prevWeapon == extent)
				this.prevWeapon = null;
			thePlayer.inventory.splice(thePlayer.inventory.indexOf(extent), 1);
			Game.theWorld.updateInventory();
		}
		onabilitystate(ability: number, state: boolean)
		{
			var slot = QuickBarSlots.indexOf(ability);
			if (state)
				theWorld.quickBarTimers[slot] = [0, 0];
			else
				theWorld.quickBarTimers[slot] = [AbilityDelays[ability], AbilityDelays[ability]];
		}
		onenchantment(extent: number, enchantment: number)
		{
			var player: PlayerObject = <PlayerObject>theWorld.get(extent);
			player.enchantment = enchantment;
		}
		onwallopen(id: number)
		{
			var wall = theWorld.secretWalls[id];
			if (wall)
				wall.sprite.visible = false;
		}
		onwallclose(id: number)
		{
			var wall = theWorld.secretWalls[id];
			if (wall)
				wall.sprite.visible = true;			
		}
		onhealth(health: number, maxhealth?: number)
		{
			if (maxhealth !== undefined)
				this.maxHealth = maxhealth;
			console.log(health);
			this.currentHealth = health;
			
			theWorld.updateHealthBar(1.0 * this.currentHealth / this.maxHealth);
		}
		onhealthdelta(extent: number, delta: number)
		{
			var player: PlayerObject = <PlayerObject>theWorld.get(extent);
			player.addHealthDelta(delta);
		}
		onstamina(stamina: number)
		{
			thePlayer.updateStamina(stamina);
		}
		oninformkill(attacker1: number, attacker2: number, victim: number)
		{
			var oa1 = theWorld.get(attacker1),
				oa2 = theWorld.get(attacker2),
				ov = theWorld.get(victim);
			if (ov instanceof PlayerObject)
			{
				if (oa1 && oa1 instanceof PlayerObject)
					theChat.addKill(ov.player.name, oa1.player.name, ov.player == thePlayer, oa1.player == thePlayer);
				else
					theChat.addKill(ov.player.name, 'nasty thing', ov.player == thePlayer);
			}
		}
		oninformmsg(id: string)
		{
			if (theStrings.messages[id])
			{
				theWorld.displayMessage(theStrings.messages[id][Math.floor(Math.random() * theStrings.messages[id].length)]);
			}
			else
			{
				theWorld.displayMessage(id);
			}
		}
		onfxdeathray(x1: number, y1: number, x2: number, y2: number)
		{
			var dx = x1 - x2,
				dy = y1 - y2,
				d = Math.sqrt(dx*dx + dy*dy),
				angle = Math.atan2(dy, dx);
			for (var i = 0; i < d; i += 2)
			{
				var px = x2 + i * Math.cos(angle) + Math.floor(Math.random() * 20) - 10,
					py = y2 + i * Math.sin(angle) + Math.floor(Math.random() * 20) - 10;
				Game.theWorld.stage.addChild(new Game.SparkDrawable('#ff00ff', px, py));
			}
		}
		onfxduration(type: number, extent1: number, extent2: number)
		{
			if (type < 8)
			{
				theWorld.addEffect(new DurationEffect(type, extent1, extent2));
			}
			else
			{
				type -= 8;
				for (var i = 0; i < theWorld.effects.length; i++)
				{
					var effect = theWorld.effects[i];
					if (effect instanceof DurationEffect)
					{
						if (effect.type == type && effect.extent1 == extent1 && effect.extent2 == extent2)
						{
							theWorld.effects.splice(i, 1);
							break;
						}
					}
				}
			}
		}
		onforgetdrawables(timestamp: number)
		{
			for (var i = 0; i < theWorld.objectsArray.length; i++)
			{
				var obj = theWorld.objectsArray[i];
				
				if (obj.extent < 0x8000 && obj.lastUpdate && obj.lastUpdate.frame < timestamp && !(obj instanceof PlayerObject))
				{
					delete theWorld.objects[obj.extent];
					theWorld.objectsArray.splice(i, 1);
					theWorld.stage.removeChild(obj);
					i--;
				}
			}
		}
		sendquit()
		{
			this.wsclient.send(new Uint8Array([0xAE]), true);
			this.wsclient.send(new Uint8Array([0x0A]));
		}
		sendtext(msg: string)
		{
			var extent = thePlayer ? thePlayer.object.extent : 0;
			var x = thePlayer ? thePlayer.object.x : 0;
			var y = thePlayer ? thePlayer.object.y : 0;
			var flags = 0x04;
			var length = msg.length + 1;
			var buf = [0xA8, extent & 0xFF, extent >> 8, flags, x & 0xFF, x >> 8, y & 0xFF, y >> 8, length & 0xFF, length >> 8, 0];
			for (var i = 0; i < msg.length; i++)
			{
				buf.push(msg.charCodeAt(i) & 0xff);
				buf.push(msg.charCodeAt(i) >> 8);
			}
			buf.push(0);
			buf.push(0);
			
			this.wsclient.send(new Uint8Array(buf));
		}
		sendability(id: number)
		{
			if (!thePlayer)
				return;
			
			this.wsclient.send(new Uint8Array([0x7A, id]));
		}
		senduse(id: number)
		{
			if (!thePlayer)
				return;
			
			this.wsclient.send(new Uint8Array([0x74, id & 0xff, (id >> 8) & 0xff]));
		}
		sendequip(id: number)
		{
			if (!thePlayer)
				return;
			
			this.wsclient.send(new Uint8Array([0x75, id & 0xff, (id >> 8) & 0xff]));
		}
		senddequip(id: number)
		{
			if (!thePlayer)
				return;
			
			this.wsclient.send(new Uint8Array([0x76, id & 0xff, (id >> 8) & 0xff]));
		}
		sendpickup(id: number)
		{
			if (!thePlayer)
				return;
			
			this.wsclient.send(new Uint8Array([0x73, id & 0xff, (id >> 8) & 0xff]));
		}
		sendupdate(mouse: {x: number, y: number})
		{
			if (!thePlayer)
				return;
			
			var buffer = new Uint8Array(64);
			var idx = 0;
			
			// send current orientation
			var orientation = (Math.atan2(mouse.y - thePlayer.object.y, mouse.x - thePlayer.object.x) + Math.PI) / (2 * Math.PI);
			orientation = (Math.max(Math.min(Math.floor(orientation * 255), 255), 0) + 128) & 0xFF;
			buffer[idx++] = 0x3F;
			buffer[idx++] = 0x00; // placeholder
			buffer[idx++] = 0x01;
			buffer[idx++] = 0x00;
			buffer[idx++] = 0x00;
			buffer[idx++] = 0x00;
			buffer[idx++] = orientation;
			
			// send actions
			if (this.pendingActions & Action.Attack)
			{
				buffer[idx++] = 0x06;
				buffer[idx++] = 0x00;
				buffer[idx++] = 0x00;
				buffer[idx++] = 0x00;
			}
			if (this.pendingActions & Action.Jump)
			{
				buffer[idx++] = 0x07;
				buffer[idx++] = 0x00;
				buffer[idx++] = 0x00;
				buffer[idx++] = 0x00;
			}
			if (this.pendingActions & Action.Move)
			{
				buffer[idx++] = 0x02;
				buffer[idx++] = 0x00;
				buffer[idx++] = 0x00;
				buffer[idx++] = 0x00;
				buffer[idx++] = 0x00; // ???
			}
			this.pendingActions = 0;
			
			// update length field
			buffer[1] = idx - 2;
				
			if (mouse.x != this.prevMouse.x || mouse.y != this.prevMouse.y)
			{
				buffer[idx++] = 0xAC;
				buffer[idx++] = mouse.x & 0xFF;
				buffer[idx++] = mouse.x >> 8;
				buffer[idx++] = mouse.y & 0xFF;
				buffer[idx++] = mouse.y >> 8;
				this.prevMouse = mouse;
			}
			
			this.wsclient.send(buffer.subarray(0, idx));
		}
		attack()
		{
			this.pendingActions |= Action.Attack;
		}
		jump()
		{
			this.pendingActions |= Action.Jump;
		}
		move()
		{
			this.pendingActions |= Action.Move;
		}
		swapweapon()
		{
			var cur = thePlayer.equipment.filter((extent) => (theWorld.get(extent).getThing().cls & ThingClass.WEAPON) != 0)[0];
			if (this.prevWeapon != null)
			{
				this.sendequip(this.prevWeapon);
			}
			else if (cur)
			{
				this.senddequip(cur);
			}
			if (cur)
				this.prevWeapon = cur;
			else
				this.prevWeapon = null;
		}
	}
	
	export class Object implements IDrawable
	{
		extent: number;
		type: number;
		x: number;
		y: number;
		visible: boolean;
		angle: number;
		d: number;
		dx: number;
		dy: number;
		lastUpdate: { frame: number, x: number, y: number };
		orientation: number;
		currentFrame: number;
		sprite: PIXI.DisplayObject;
		_sprite: PIXI.Sprite;
		frameTimer: number;
		frameTimerReset: number;
		numFrames: number;
		charges: {total: number, current: number};
		
		constructor(extent: number, type: number)
		{
			this.x = 0;
			this.y = 0;
			this.angle = 0;
			this.d = Infinity;
			this.visible = false;
			this.extent = extent;
			this.type = type;
			this.orientation = 0;
			this.currentFrame = 0;
			this.frameTimerReset = this.getThing().frameTimer * 2;
			this.frameTimer = this.frameTimerReset;
			this.charges = null;
			
			this.sprite = this._sprite = new PIXI.Sprite();
			
			var thing = this.getThing();
			if (thing.flags & ThingFlags.EDIT_VISIBLE)
				this.sprite.visible = false;

			switch (thing.draw)
			{
				case 'AnimateDraw':
				case 'SlaveDraw':
				case 'DoorDraw':
				case 'WeaponAnimateDraw':
					this.numFrames = this.getThing().images.length;
					break;
				case 'VectorAnimateDraw':
					this.numFrames = this.getThing().images.length / 8;
					break;
				default:
					this.numFrames = 1;
					break;
			}
			
			this.dx = 0;
			this.dy = 0;
			this.lastUpdate = null;
		}
		draw()
		{
			var thing = this.getThing();
			if (thing.draw == 'AnimateDraw'
				|| thing.draw == 'VectorAnimateDraw'
				|| thing.draw == 'WeaponAnimateDraw')
			{
				this.frameTimer--;
				if (this.frameTimer <= 0)
				{
					this.currentFrame = (this.currentFrame + 1) % this.numFrames;
					this.frameTimer = this.frameTimerReset;
				}
			}			
			if (thing.draw == 'VectorAnimateDraw')
			{
				var numFrames = thing.images.length / 8;
				if (thing.images[this.orientation * numFrames + this.currentFrame])
					this._sprite.texture = getimage(thing.images[this.orientation * numFrames + this.currentFrame]);
			}
			else if (thing.draw == 'SlaveDraw' || thing.draw == 'DoorDraw' || thing.draw == 'AnimateDraw' || thing.draw == 'WeaponAnimateDraw' || thing.draw == 'HarpoonDraw')
			{
				if (thing.images[this.currentFrame])
					this._sprite.texture = getimage(thing.images[this.currentFrame]);
			}
			else if (thing.draw == 'NoDraw')
			{
				this._sprite.visible = false;
			}
			else if (!this._sprite.texture.valid)
			{
				if (thing.images.length > 0)
				{
					this._sprite.texture = getimage(thing.images[0]);
				}
				else
				{
					var g = new PIXI.Graphics();
					g.beginFill(0xFFFFFF);
					g.drawRect(0, 0, 20, 20);
					g.endFill();
					var text = new PIXI.Text(this.extent.toString(), {font: '10px serif', fill: 'black'});
					text.x = 0;
					text.y = 0;
					var sprite = new PIXI.Container();
					sprite.addChild(g);
					sprite.addChild(text);
					this._sprite.texture = sprite.generateTexture(renderer, PIXI.SCALE_MODES.LINEAR, 1.0);				
				}
			}
		}
		getThing() : JThing
		{
			return theThings.objects[this.type];
		}
		update(x: number, y: number, other?: Uint8Array) : void
		{
			var thing = this.getThing();
			
			if (this.lastUpdate != null)
			{
				var dt = 1.0 * (theClient.wsclient.timestamp - this.lastUpdate.frame);
				if (dt > 0)
				{
					this.dx = (x - this.lastUpdate.x) / dt;
					this.dy = (y - this.lastUpdate.y) / dt;
				}
				else
				{
					this.dx = 0;
					this.dy = 0;
				}
			}
			this.lastUpdate = {frame: theClient.wsclient.timestamp, x: x, y: y};
			
			this.x = x;
			if (this.y != y)
			{
				this.y = y;
				theWorld.stage.updateChild(this);
			}
			
			this.sprite.x = this.x - Math.floor(thing.width / 2);
			this.sprite.y = this.y - Math.floor(thing.height / 2) - thing.z;
			
			if (!thing.images.length && thing.playerImages === null)
			{
				this.sprite.x = this.x - 10;
				this.sprite.y = this.y - 10;
			}
		}
	}
	
	export class ProjectileObject extends Object
	{
		orientation: number;
		update(x: number, y: number, other?: Uint8Array) : void
		{
			super.update(x, y);
			this.orientation = (other[0] >> 4) & 0x7;
		}
	}	
	
	export class HarpoonObject extends Object
	{
		orientation: number;
		update(x: number, y: number, other?: Uint8Array) : void
		{
			super.update(x, y);
			this.orientation = (other[0] >> 4) & 0x7;
			this.currentFrame = other[1];
		}
	}
	
	export class PlayerObject extends Object
	{
		player: Player;
		stance: number;
		numFrames: number;
		delay: number;
		redraw: boolean;
		obs: boolean;
		loaded: boolean;
		armor: number;
		weapon: number;
		enchantment: number;
		canvas: HTMLCanvasElement;
		canvasContext: CanvasRenderingContext2D;
		nameText: PIXI.Text;
		healthDeltas: PIXI.Text[];
		constructor(extent: number, type: number)
		{
			super(extent, type);
			
			this.delay = 0;
			this.stance = 0;
			this.redraw = true;
			this.loaded = false;
			this.obs = false;
			this.armor = 0;
			this.weapon = 0;
			this.healthDeltas = [];
			
			this.canvas = document.createElement('canvas');
			this.canvasContext = <CanvasRenderingContext2D>this.canvas.getContext('2d');
			this._sprite.texture = PIXI.Texture.fromCanvas(this.canvas);
			
			this.sprite = new PIXI.Container();
			(<PIXI.Container>this.sprite).addChild(this._sprite);
			
			this.nameText = new PIXI.Text('', { font: '8pt monospace', fill: '#CCCCCC' });
			(<PIXI.Container>this.sprite).addChild(this.nameText);
		}
		setName(name: string)
		{
			this.nameText.text = name;
			this.nameText.x = 64 - this.nameText.width/2;
			this.nameText.y = 10;
		}
		addHealthDelta(delta: number)
		{
			var text = new PIXI.Text('', {font: 'bold 8pt monospace', fill: this == thePlayer.object ? 'red' : 'yellow', stroke: 'black', strokeThickness: 2});
			text.x = 56;
			text.y = 40;
			text.text = (-delta).toString();
			this.healthDeltas.push(text);
			(<PIXI.Container>this.sprite).addChild(text);
		}
		update(x: number, y: number, other?: Uint8Array) : void
		{
			super.update(x, y);
			
			var orientation = (other[0] >> 4) & 0x7;
			var stance;
			if (other[0] & 0x80)
			{
				stance = other[2];
			}
			else
			{
				stance = other[1];
			}
			
			if (orientation != this.orientation || stance != this.stance)
			{
				var thing = this.getThing();
				
				if (other[0] & 0x80)
					this.currentFrame = other[1];
				
				this.orientation = orientation;
				this.stance = stance;
				this.delay = thing.playerImages[this.stance].delay + 1;
				this.numFrames = (thing.playerImages[this.stance].naked.length) / 8;
				this.currentFrame %= this.numFrames;
				this.redraw = true;
			}
		}
		draw()
		{
			var thing = this.getThing();				
			var stance = this.stance;
			if (stance == PlayerStance.WALK && (this.enchantment & (1 << Enchant.ENCHANT_SNEAK)))
				stance = PlayerStance.SNEAK;
			
			if (this.player && this.obs)
			{
				if (this.player == thePlayer)
					this.sprite.alpha = 0.4;
				else
				{
					this.sprite.visible = false;
					return;
				}
			}
			else
			{
				this.sprite.alpha = 1.0;
			}
			
			if (this.player == thePlayer)
			{
				this.x += this.dx;
				this.y += this.dy;
				this.sprite.x += this.dx;
				this.sprite.y += this.dy;
			}
			
			if (this.numFrames != 0)
			{
				this.delay -= 1;
				if (this.delay == 0)
				{
					if (thing.playerImages[stance].type == 'Loop')
						this.currentFrame = (this.currentFrame + 1) % this.numFrames;
					else if (this.currentFrame < this.numFrames - 1)
						this.currentFrame++;
					this.redraw = true;
				}
			}
			if (this.redraw)
			{
				if (this.orientation >= 8 || this.type == 0 || !thing.playerImages[this.stance])
				{
					this.sprite.visible = false;
					return;
				}
				
				var imageData = this.canvasContext.createImageData(128, 128);
				drawimage(imageData, thing.playerImages[stance].naked[this.orientation * this.numFrames + this.currentFrame]);
				for (var i = 0; i < 32; i++)
				{
					if ((1 << i) == PlayerArmor.MEDIEVAL_CLOAK) continue; // draw this last
					if ((this.armor & (1 << i)) && thing.playerImages[stance].armor[1 << i])
						drawimage(imageData, thing.playerImages[stance].armor[1 << i][this.orientation * this.numFrames + this.currentFrame]);
				}
				if ((this.armor & PlayerArmor.MEDIEVAL_CLOAK) &&  thing.playerImages[stance].armor[PlayerArmor.MEDIEVAL_CLOAK])
					drawimage(imageData, thing.playerImages[stance].armor[PlayerArmor.MEDIEVAL_CLOAK][this.orientation * this.numFrames + this.currentFrame]);
					
				for (var i = 0; i < 32; i++)
					if ((this.weapon & (1 << i)) && thing.playerImages[stance].weapon[1 << i])
						drawimage(imageData, thing.playerImages[stance].weapon[1 << i][this.orientation * this.numFrames + this.currentFrame]);
				this.canvasContext.putImageData(imageData, 0, 0);
				this.sprite.visible = true;
				this.redraw = false;
				this.delay = thing.playerImages[stance].delay + 1;
			}
			
			// update health texts
			var toRemove = [];
			for (var i = 0; i < this.healthDeltas.length; i++)
			{
				var text = this.healthDeltas[i];
				if (text.y <= -10)
				{
					toRemove.push(i);
				}
				else
				{
					text.y -= 2;
				}
			}
			for (var i = 0; i < toRemove.length; i++)
			{
				(<PIXI.Container>this.sprite).removeChild(this.healthDeltas[i]);
				this.healthDeltas.splice(toRemove[i], 1);
			}
			
			// update enchantment effects
			if (this.enchantment & (1 << Enchant.ENCHANT_INFRAVISION))
			{
				var n = Math.floor(Math.random() * 10);
				for (var i = 0; i < n; i++)
				{
	                var dx = Math.floor(Math.random() * 10);
	                var dy = Math.floor(Math.random() * 10);
                	Game.theWorld.addEffect(new ParticleEffect(0x00ff00, this.x + dx - 2, this.y + dy - 40, 15, 2));
				}
			}
		}
		equiparmor(bitmask: number, equip: boolean)
		{
			if (equip)
				this.armor |= bitmask;
			else
				this.armor &= ~bitmask;
		}
		equipweapon(bitmask: number, equip: boolean)
		{
			if (equip)
				this.weapon |= bitmask;
			else
				this.weapon &= ~bitmask;
		}
		setobs(mode: boolean)
		{
			this.loaded = true;
			if (this.obs != mode)
			{
				this.obs = mode;
				this.redraw = true;
			}
		}
	}
	
	export class Player
	{
		object: PlayerObject;
		cls: number;
		name: string;
		wolname: string;
		respawnArmor: number;
		respawnWeapon: number;
		inventory: number[];
		equipment: number[];
		stamina: number;
		constructor(extent: number, cls: number, name: string, wolname: string, respawnArmor: number, respawnWeapon: number)
		{
			this.object = <PlayerObject>theWorld.getOrCreate(extent, 0x2C9);
			this.object.player = this;
			this.object.setName(name);
			
			this.cls = cls;
			this.name = name;
			this.wolname = wolname;
			
			switch (cls)
			{
				case PlayerClass.WARRIOR:
					respawnArmor = PlayerArmor.STREET_PANTS | PlayerArmor.STREET_SNEAKERS | PlayerArmor.ROUND_SHIELD;
					respawnWeapon = PlayerWeapon.LONG_SWORD;
					break;
				case PlayerClass.WIZARD:
					respawnArmor = PlayerArmor.STREET_PANTS | PlayerArmor.STREET_SNEAKERS | PlayerArmor.STREET_SHIRT | PlayerArmor.WIZARD_ROBE;
					respawnWeapon = 0;
					break;
				case PlayerClass.CONJURER:
					respawnArmor = PlayerArmor.STREET_PANTS | PlayerArmor.STREET_SNEAKERS | PlayerArmor.STREET_SHIRT;
					respawnWeapon = 0;
					break;
			}
			
			this.inventory = new Array<number>();
			this.equipment = new Array<number>();
			this.object.armor = this.respawnArmor = respawnArmor;
			this.object.weapon = this.respawnWeapon = respawnWeapon;
		}
		usePoison()
		{
			var extent = _.find(this.inventory, (extent) => theWorld.get(extent).type == 631);
			if (extent)
				theClient.senduse(extent);
		}
		useHealth()
		{
			for (var i = 0; i < HealthPotions.length; i++)
			{
				var type = HealthPotions[i];
				var extent = _.find(this.inventory, (extent) => theWorld.get(extent).type == type);
				if (extent)
					theClient.senduse(extent);
			}
		}
		useMana()
		{
			var extent = _.find(this.inventory, (extent) => theWorld.get(extent).type == 638);
			if (extent)
				theClient.senduse(extent);
		}
		updateStamina(stamina?: number)
		{
			if (stamina == null)
			{
				if (this.stamina != 100)
				{
					this.stamina += 3;
					if (this.stamina > 100) this.stamina = 100;
					theWorld.updateStamina();
				}
			}
			else
			{
				this.stamina = stamina;
				theWorld.updateStamina();
			}
		}
	}
	
	export class ThingDb
	{
		objects: { [id: number]: JThing };
		tiles: { [id: number]: JTile };
		walls: { [id: number]: JWall };
		constructor(db: JThingDb)
		{
			this.objects = {};
			this.tiles = {};
			this.walls = {};
			
			db.things.forEach((thing: JThing) => {
				this.objects[thing.id] = thing;
			});
			db.tiles.forEach((tile: JTile) => {
				this.tiles[tile.id] = tile;
			});
			db.walls.forEach((wall: JWall) => {
				this.walls[wall.id] = wall;
			});
		}
	}
	
	export class Wall implements IDrawable
	{
		mapWall: JMapWall;
		wall: JWall;
		sprite: PIXI.Sprite;
		x: number;
		y: number;
		visible: boolean;
		angle: number;
		d: number;
		facing: number;
		window: boolean;
		constructor(mapWall: JMapWall)
		{
			this.visible = false;
			this.mapWall = mapWall;
			this.wall = theThings.walls[mapWall.id];
			var facing: number = mapWall.facing;
			this.facing = facing;
			this.window = mapWall.window;
			if (mapWall.window)
			{
				if (facing == WallFacing.UP)
					facing = WallFacing.WINDOWUP;
				else if (facing == WallFacing.DOWN)
					facing = WallFacing.WINDOWDOWN; 
			}
			this.sprite = new PIXI.Sprite(getimage(this.wall.images[facing][mapWall.variation][0].image));
			this.x = mapWall.x * 23;
			this.y = mapWall.y * 23;
			this.sprite.x = this.x - 52 - this.wall.images[facing][mapWall.variation][0].x; // XXX appears to be off by 4 or so
			this.sprite.y = this.y - 72 - this.wall.images[facing][mapWall.variation][0].y;
		}
		draw()
		{
			var angle = Math.atan2(Game.thePlayer.object.y - this.y, Game.thePlayer.object.x - this.x) / Math.PI;
			var bottom = false;
			if (this.facing == WallFacing.DOWN)
				bottom = angle >= -0.75 && angle < 0.25;
			else if (this.facing == WallFacing.UP)
				bottom = !(angle >= -0.25 && angle < 0.75);
			else if (this.facing == WallFacing.ARROWDOWN || this.facing == WallFacing.TNORTHWEST || this.facing == WallFacing.TNORTHEAST)
				bottom = angle >= -0.75 && angle < -0.25;
			else if (this.facing == WallFacing.ARROWUP || this.facing == WallFacing.TSOUTHWEST || this.facing == WallFacing.TSOUTHEAST)
			 	bottom = !(angle < 0.75 && angle > 0.25);
			else if (this.facing == WallFacing.ARROWRIGHT)
				bottom = (angle < 0.25 || angle >= 0.75);
			else if (this.facing == WallFacing.ARROWLEFT)
				bottom = !(angle >= 0.25 && angle <= 0.75);
			
			if (bottom)
				this.sprite.alpha = 0.4;
			else
				this.sprite.alpha = 1.0;			
			
			var newfacing = null;
			if (this.facing == WallFacing.TNORTHWEST)
			{
				if (angle >= -0.75 && angle <= -0.25)
					newfacing = WallFacing.ARROWDOWN;
				else if (angle <= -0.75 || angle >= 0.75)
					newfacing = WallFacing.ARROWRIGHT;
				else
					newfacing = WallFacing.UP;
			}
			if (this.facing == WallFacing.TNORTHEAST)
			{
				if (angle >= -0.75 && angle <= -0.25)
					newfacing = WallFacing.ARROWDOWN;
				else if (angle >= -0.25 && angle <= 0.25)
					newfacing = WallFacing.ARROWLEFT;
				else
					newfacing = WallFacing.DOWN;
			}
			if (this.facing == WallFacing.TSOUTHEAST)
			{
				if (angle >= -0.25 && angle <= 0.25)
					newfacing = WallFacing.ARROWLEFT;
				else if (angle >= 0.25 && angle <= 0.75)
					newfacing = WallFacing.ARROWUP;
				else
					newfacing = WallFacing.UP;
			}
			if (this.facing == WallFacing.TSOUTHWEST)
			{
				if (angle >= 0.25 && angle <= 0.75)
					newfacing = WallFacing.ARROWUP;
				else if (angle <= -0.75 || angle >= 0.75)
					newfacing = WallFacing.ARROWRIGHT;
				else
					newfacing = WallFacing.DOWN;
			}
			if (newfacing !== null)
				this.sprite.texture = getimage(this.wall.images[newfacing][this.mapWall.variation][0].image);
		}
	}

	export class World
	{
		players: { [extent: number]: Player };
		objects: { [extent: number]: Object };
		effects: Effect[];
		objectsArray: Object[];
		immobileObjects: { [extent: number]: Object };
		walls: { [id: number]: Wall };
		wallsArray: Wall[];
		secretWalls: { [id: number]: Wall };
		lastUpdate: number;
		currentFrame: number;
		targetingAbility: number;
		im: PIXI.interaction.InteractionManager;
		renderer: PIXI.CanvasRenderer | PIXI.WebGLRenderer;
		belowstage: PIXI.Container;
		stage: Container;
		effectstage: PIXI.Container;
		effectGraphic: PIXI.Graphics;
		allstage: PIXI.Container;
		guistage: PIXI.Container;
		infoText: PIXI.Text;
		pickupCircle: PIXI.Graphics;
		healthBar: PIXI.Graphics;
		potionUi: PIXI.Container;
		potionPoisonImage: PIXI.Sprite;
		potionPoisonCount: PIXI.Text;
		potionHealthImage: PIXI.Sprite;
		potionHealthCount: PIXI.Text;
		weaponChargesText: PIXI.Text;
		weaponImage: PIXI.Sprite;
		staminaCircle: PIXI.Graphics;
		inventoryScreenPage: number;
		inventoryScreen: PIXI.Container;
		inventoryScreenItems: PIXI.Container;
		inventoryScreenModel: PIXI.Container;
		inventoryScreenHandle: PIXI.Sprite;
		inventoryScreenUp: PIXI.Sprite;
		inventoryScreenDown: PIXI.Sprite;
		quickBarUi: PIXI.Container;
		quickBarImages: PIXI.Sprite[];
		quickBarOverlays: PIXI.Graphics[];
		quickBarTimers: number[][];
		overlay: PIXI.Graphics;
		messages: { msg: string, timestamp: number, o: PIXI.Text }[];
		messagesContainer: PIXI.Container;
		toolTipContainer: PIXI.Container;
		toolTipBox: PIXI.Graphics;
		toolTipText: PIXI.Text;
		idleTimer: number;
		
		constructor()
		{
			this.lastUpdate = 0;
			this.objects = {};
			this.players = {};
			this.effects = [];
			this.objectsArray = [];
			this.wallsArray = [];
			this.currentFrame = 0;
			this.targetingAbility = null;
			this.idleTimer = Date.now();
		}
		addEffect(effect: Effect)
		{
			this.effects.push(effect);
		}
		addPlayer(player: Player)
		{
			this.players[player.object.extent] = player;
		}
		get(extent: number)
		{
			return this.objects[extent];
		}
		getOrCreate(extent: number, type: number)
		{
			if (this.objects[extent] === undefined)
			{
				var o = createobject(extent, type);				
				this.objects[extent] = o;
				this.objectsArray.push(o);
					
				if (o.getThing().flags & ThingFlags.BELOW)
					this.belowstage.addChild(o.sprite);
				else
					this.stage.addChild(o);
				
				return o;
			}
				
			return this.objects[extent];
		}
		findObjects(loc: {x: number, y: number}, radius: number = 30): Object[]
		{
			var radius2 = radius * radius;
			var result: Object[] = new Array<Object>();
			var idx = _.sortedIndex(this.stage.children, <IDrawable>{y: loc.y-radius-1}, (x) => x.y);
			for (; idx < this.stage.children.length; ++idx)
			{
				var o = this.stage.children[idx];
				if (o instanceof Object)
				{
					if (Math.pow(loc.y - o.y, 2) + Math.pow(loc.x - o.x, 2) < radius2)
						result.push(o);
				}
				
				if (o.y > loc.y + radius)
					break;
			}
			return result;
		}
		loadMap(mapname: string)
		{
			theAssets.load(mapname + ".json", (url, response) => {
				var map: JMap = JSON.parse(response);
				this.processMap(map);
				return true;
			});
		}
		processMap(map: JMap)
		{
			this.walls = {};
			this.secretWalls = {};
			
			this.belowstage.cacheAsBitmap = false;
			map.tiles.forEach((mapTile) => {
				var tile = theThings.tiles[mapTile.id];
				var sprite = new PIXI.Sprite(getimage(tile.images[mapTile.variation]));
				
				sprite.x = mapTile.x * 23 - 11;
				sprite.y = mapTile.y * 23 + 11;
				
				this.belowstage.addChild(sprite);
			});
			
			map.walls.forEach((mapWall, idx) => {
				var wall = new Wall(mapWall);
				this.stage.addChild(wall);
				
				this.walls[idx] = wall;
				this.wallsArray.push(wall);
				if (mapWall.secret)
					this.secretWalls[mapWall.secretId] = wall; 
			});
			
			map.objects.forEach((obj: JMapObject) => {
				if (theThings.objects[obj.type].cls & ThingClass.IMMOBILE)
				{
					// IMMOBILE extents overlap with other object extents
					var o = this.getOrCreate(obj.extent | 0x8000, obj.type);
					o.update(obj.x, obj.y);
					o.draw();
				}
			});
			
			// wait for textures to load...
			window.setTimeout(() => {
				this.belowstage.cacheAsBitmap = true;
			}, 10000);
		}
		createUi()
		{
			var createImage = (x: number, y: number, image: number) => {
				var sprite = new PIXI.Sprite(getimage(image));
				sprite.x = x;
				sprite.y = y;
				return sprite;
			};
			var createText = (msg: string, font: PIXI.TextStyle, x: number, y:number) => {
				var text = new PIXI.Text(msg, font);
				text.x = x;
				text.y = y;
				return text;
			};
			this.guistage.addChild(createImage(-1, this.renderer.height - 127, 14508));
			this.guistage.addChild(createImage(this.renderer.width - 92, this.renderer.height - 202, 14453));
			this.guistage.addChild(createImage(this.renderer.width - 92, this.renderer.height - 202, 14505 /* 14448 */));
			
			this.healthBar = new PIXI.Graphics();
			this.healthBar.x = this.renderer.width - 53;
			this.healthBar.y = this.renderer.height - 169;
			this.healthBar.alpha = 0.7;
			this.guistage.addChild(this.healthBar);
			this.updateHealthBar(0);
			
			var potionPoisonHotkey = createText('Z', {font: '6pt monospace', fill: 'white'}, 936, 755);
			var potionHealthHotkey = createText('X', {font: '6pt monospace', fill: 'white'}, 963, 755);
			this.potionPoisonImage = new PIXI.Sprite();
			this.potionPoisonImage.x = 888;
			this.potionPoisonImage.y = 683;
			this.potionPoisonImage.visible = false;
			this.potionHealthImage = new PIXI.Sprite();
			this.potionHealthImage.x = 915;
			this.potionHealthImage.y = 683;
			this.potionHealthImage.visible = false;
			this.potionPoisonCount = new PIXI.Text('', {font: '6pt monospace', fill: 'white'});
			this.potionPoisonCount.x = 936;
			this.potionPoisonCount.y = 729;
			this.potionHealthCount = new PIXI.Text('', {font: '6pt monospace', fill: 'white'});
			this.potionHealthCount.x = 963;
			this.potionHealthCount.y = 729;
			this.potionUi = new PIXI.Container();
			this.potionUi.addChild(this.potionPoisonImage);
			this.potionUi.addChild(this.potionHealthImage);
			this.potionUi.addChild(this.potionPoisonCount);
			this.potionUi.addChild(this.potionHealthCount);
			this.potionUi.addChild(potionPoisonHotkey);
			this.potionUi.addChild(potionHealthHotkey);
			this.potionUi.cacheAsBitmap = true;
			this.guistage.addChild(this.potionUi);
			
			this.staminaCircle = new PIXI.Graphics();
			this.staminaCircle.x = 50;
			this.staminaCircle.y = 719;
			this.guistage.addChild(this.staminaCircle);
			var weaponHotkey = createText('V', {font: '6pt monospace', fill: 'white'}, 18, 742);
			this.weaponImage = new PIXI.Sprite();
			this.weaponChargesText = createText('', {font: '6pt monospace', fill: 'white'}, 69, 672);
			this.weaponChargesText.anchor = new PIXI.Point(0.5, 0.5);
			this.guistage.addChild(this.weaponImage);
			this.guistage.addChild(weaponHotkey);
			this.guistage.addChild(this.weaponChargesText);
			
			this.quickBarUi = new PIXI.Container();
			this.quickBarUi.x = 360;
			this.quickBarUi.y = 693;
			this.quickBarUi.addChild(createImage(0, 0, 14415));
			this.quickBarUi.addChild(createImage(0, 0, 14446));
			this.quickBarUi.addChild(createImage(0, 0, 14447));
			this.quickBarImages = [];
			this.quickBarOverlays = [];
			this.quickBarTimers = [];
			for (var i = 0; i < 5; i++)
			{
				var sprite = new PIXI.Sprite();
				sprite.x = 71 + i * 37;
				sprite.y = 34;
				sprite.texture = getimage(AbilityImages[QuickBarSlots[i]][0]);
				this.quickBarImages.push(sprite);
				this.quickBarUi.addChild(sprite);
				
				var g = new PIXI.Graphics();
				g.x = 71 + i * 37;
				g.y = 34;
				this.quickBarOverlays.push(g);
				this.quickBarUi.addChild(g);
				
				this.quickBarTimers.push([0, 0]);
			}
			this.quickBarUi.addChild(createImage(0, 0, 14424));
			this.quickBarUi.addChild(createText('A', {font: '6pt monospace', fill: 'white'}, 83, 62));
			this.quickBarUi.addChild(createImage(0, 0, 14425));
			this.quickBarUi.addChild(createText('S', {font: '6pt monospace', fill: 'white'}, 120, 62));
			this.quickBarUi.addChild(createImage(0, 0, 14426));
			this.quickBarUi.addChild(createText('D', {font: '6pt monospace', fill: 'white'}, 158, 62));
			this.quickBarUi.addChild(createImage(0, 0, 14427));
			this.quickBarUi.addChild(createText('F', {font: '6pt monospace', fill: 'white'}, 195, 62));
			this.quickBarUi.addChild(createImage(0, 0, 14428));
			this.quickBarUi.addChild(createText('G', {font: '6pt monospace', fill: 'white'}, 232, 62));
			this.guistage.addChild(this.quickBarUi);
			
			this.inventoryScreenPage = 0;
			this.inventoryScreen = new PIXI.Container();
			this.inventoryScreen.visible = false;
			this.inventoryScreenModel = new PIXI.Container();
			this.inventoryScreenModel.x = 10;
			this.inventoryScreenModel.y = 10;
			this.inventoryScreen.addChild(this.inventoryScreenModel);
			this.inventoryScreen.addChild(createImage(0, 0, 14755));
			this.inventoryScreen.addChild(createImage(254, 13, 14775));
			this.inventoryScreen.addChild(createImage(314, 13, 14757));
			this.inventoryScreen.addChild(createImage(314, 63, 14763));
			this.inventoryScreen.addChild(createImage(314, 113, 14767));
			this.inventoryScreenItems = new PIXI.Container();
			this.inventoryScreenItems.x = 314;
			this.inventoryScreenItems.y = 13;
			this.inventoryScreen.addChild(this.inventoryScreenItems);	
			this.inventoryScreenHandle = createImage(522, 25, 14601);
			this.inventoryScreenUp = createImage(522, 0, 14778);
			this.inventoryScreenDown = createImage(522, 150, 14780);
			this.inventoryScreen.addChild(this.inventoryScreenHandle);	
			this.inventoryScreen.addChild(this.inventoryScreenUp);
			this.inventoryScreen.addChild(this.inventoryScreenDown);	
			this.guistage.addChild(this.inventoryScreen);
			
			this.messages = [];
			this.messagesContainer = new PIXI.Container();
			this.guistage.addChild(this.messagesContainer);
			
			this.toolTipContainer = new PIXI.Container();
			this.toolTipBox = new PIXI.Graphics();
			this.toolTipText = new PIXI.Text('', {font: '8px monospace', fill: 'yellow'});
			this.toolTipContainer.addChild(this.toolTipBox);
			this.toolTipContainer.addChild(this.toolTipText);
			this.guistage.addChild(this.toolTipContainer);
			
			this.updateInventory();
		}
		updateAbilities()
		{
			for (var i = 0; i < 5; i++)
			{
				var overlay = this.quickBarOverlays[i];
				var timer = this.quickBarTimers[i];
				
				if (timer[0] == 0)
				{
					overlay.visible = false;
					continue;
				}
				
				this.quickBarTimers[i][0] = timer[0] - 1;
				overlay.visible = true;
				overlay.clear();
				overlay.beginFill(0x000000, 0.5)
					.drawRect(0, 0, 30, Math.floor(timer[0] / timer[1] * 30))
					.endFill();
			}
		}
		updateHealthBar(percent: number)
		{
			this.healthBar.clear();
			this.healthBar.beginFill(0x000000, 0.5);
			this.healthBar.drawRect(0, 0, 15, 126);
			this.healthBar.endFill();
			this.healthBar.beginFill(0xD00000, 0.7);
			this.healthBar.drawRect(0, Math.floor(126 * (1 - percent)), 15, Math.floor(126 * percent));
			this.healthBar.endFill();
		}
		displayMessage(_msg: string)
		{
			var m = {msg: _msg, timestamp: this.currentFrame, o: new PIXI.Text(_msg, {font: '10px monospace', fill: 'white', stroke: 'black', strokeThickness: 1})};
			m.o.anchor = new PIXI.Point(0.5, 0.5);
			if (this.messages.length >= 3)
				this.messages.splice(0, 1);
			this.messages.push(m);
			
			theChat.addMessage(_msg);
		}
		updateMessages()
		{
			for (var i = 0; i < this.messages.length; i++)
			{
				var m = this.messages[i];
				var remaining = (m.timestamp + 90) - this.currentFrame;
				
				if (remaining <= 0)
				{
					this.messages.splice(0, 1);
					i--;
					continue;
				}
			}
			
			this.messagesContainer.removeChildren();
			for (var i = 0; i < this.messages.length; i++)
			{
				var m = this.messages[i];
				this.messagesContainer.addChild(m.o);
				m.o.x = 512;
				m.o.y = 600 - 15 * i;
			}
		}
		updatePotions()
		{
			this.potionUi.cacheAsBitmap = false;
			
			// count number of each type in inventory
			var counts = _.countBy(thePlayer.inventory, (extent) => this.get(extent).type);
			
			// cure poison potions
			if (counts[631])
			{
				console.log('found ' + counts[631] + ' poison potions');
				this.potionPoisonImage.texture = getimage(theThings.objects[631].images[0]);
				this.potionPoisonImage.visible = true;
				this.potionPoisonCount.text = counts[631].toString();
			}
			else
			{
				this.potionPoisonImage.visible = false;
				this.potionPoisonCount.text = '';
			}
			
			// mana potions
			if (counts[638])
			{
				console.log('found ' + counts[638] + ' mana potions');
			}
			
			// health foods
			for (var i = 0; i < HealthPotions.length; i++)
			{
				var type = HealthPotions[i];
				if (counts[type])
				{
					console.log('found ' + counts[type] + ' health potions');	
					this.potionHealthImage.texture = getimage(theThings.objects[type].images[0]);	
					this.potionHealthImage.visible = true;
					this.potionHealthCount.text = counts[type].toString();
					break;
				}
			}
			if (i == HealthPotions.length)
			{
				this.potionHealthImage.visible = false;
				this.potionHealthCount.text = '';
			}
			
			this.potionUi.cacheAsBitmap = true;
		}
		updateStamina()
		{
			var angle = 2 * ((100 - thePlayer.stamina) / 100) - 0.5;
			this.staminaCircle.clear();
			this.staminaCircle.beginFill(0xff00ff, 0.5)
				.moveTo(0, 0)
				.arc(0, 0, 28, -0.5 * Math.PI, angle * Math.PI)
				.endFill();
		}
		hideTooltip()
		{
			this.toolTipBox.clear();
			this.toolTipText.text = '';
		}
		showTooltip(msg: string)
		{
			this.toolTipText.text = msg;
			this.toolTipBox.clear();
			this.toolTipBox.beginFill(0, 0.5)
				.drawRect(-2, -2, this.toolTipText.width + 4, this.toolTipText.height + 4)
				.endFill();
		}
		updateInventory()
		{
			if (!thePlayer)
				return;
				
			this.updatePotions();
			
			if (!this.inventoryScreen.visible)
				return;
				
			var createImage = (x: number, y: number, image: number) => {
				var sprite = new PIXI.Sprite(getimage(image));
				sprite.x = x;
				sprite.y = y;
				return sprite;
			};
			
			this.inventoryScreenItems.removeChildren();
			this.inventoryScreenModel.removeChildren();
			
			var modelBg = new PIXI.Graphics();
			modelBg.beginFill(0);
			modelBg.drawRect(0, 0, 210, 215);
			modelBg.endFill();
			this.inventoryScreenModel.addChild(modelBg);
			this.inventoryScreenModel.addChild(createImage(0, 5, 14342));
			
			this.inventoryScreenHandle.y = 25 + this.inventoryScreenPage * 5;
			
			if (thePlayer)
			{
				for (var i = 0; i < 32; i++)
					if (thePlayer.object.armor & (1 << i))
						this.inventoryScreenModel.addChild(createImage(0, 5, ModelArmor[1 << i]));
				
				for (var i = 0; i < 32; i++)
					if (thePlayer.object.weapon & (1 << i))
						this.inventoryScreenModel.addChild(createImage(0, 5, ModelWeapon[1 << i]));
				
				var offset = this.inventoryScreenPage * 12;
				for (var row = 0; row < 3; row++)
				{
					for (var col = 0; col < 4; col++)
					{
						if (offset + row * 4 + col >= thePlayer.inventory.length)
							break;
						
						var extent = thePlayer.inventory[offset + row*4 + col];
						var o = createImage(col * 50 - 40, row * 50 - 40, this.objects[extent].getThing().images[0]);
						this.inventoryScreenItems.addChild(o);
						if (thePlayer.equipment.indexOf(extent) >= 0)
						{
							var o = createImage(col * 50, row * 50, 14672);
							this.inventoryScreenItems.addChild(o);
							o.alpha = 0.5;
						}
						else if (theClient.prevWeapon == extent)
						{
							var o = createImage(col * 50, row * 50, 14691);
							this.inventoryScreenItems.addChild(o);
							o.alpha = 0.5;							
						}
					}
				}
			}
		}
		updateWeapon()
		{
			var extent = thePlayer.equipment.filter((extent) => (theWorld.get(extent).getThing().cls & ThingClass.WEAPON) != 0)[0];
			if (!extent)
			{
				this.weaponImage.texture = getimage(14399);
				this.weaponImage.x = 23;
				this.weaponImage.y = this.renderer.height - 78;
				this.weaponChargesText.text = '';
				return;
			}
			var o: Object = theWorld.get(extent);
			this.weaponImage.texture = getimage(o.getThing().images[0]);
			this.weaponImage.x = -13;
			this.weaponImage.y = this.renderer.height - 112;
			if (o.charges)
				this.weaponChargesText.text = o.charges.current.toString();
			else
				this.weaponChargesText.text = '';
		}
		render(renderer: PIXI.CanvasRenderer | PIXI.WebGLRenderer)
		{
			this.renderer = renderer;
			this.allstage = new PIXI.Container();
			this.stage = new Container();
			this.effectstage = new PIXI.Container();
			this.effectGraphic = new PIXI.Graphics();
			this.belowstage = new PIXI.Container();
			this.guistage = new PIXI.Container();
			this.infoText = new PIXI.Text('', {font: '12px serif', fill: 'white'});
			this.overlay = new PIXI.Graphics();
			
			theGradient = (<PIXI.CanvasRenderer>renderer).context.createRadialGradient(10, 10, 10, 10, 10, 1);
			theGradient.addColorStop(0, 'white');
			theGradient.addColorStop(1, 'purple');
			
			this.pickupCircle = new PIXI.Graphics();
			this.pickupCircle.beginFill(0x880000);
			this.pickupCircle.drawCircle(0, 0, 10);
			this.pickupCircle.endFill();
			this.pickupCircle.visible = false;
			
			this.allstage.addChild(this.belowstage);
			this.allstage.addChild(this.overlay);
			this.allstage.addChild(this.pickupCircle);
			this.allstage.addChild(this.stage);
			this.allstage.addChild(this.effectstage);
			this.effectstage.addChild(this.effectGraphic);
			this.guistage.addChild(this.allstage);
			this.guistage.addChild(this.infoText);
			this.im = new PIXI.interaction.InteractionManager(this.renderer, {});
			
			this.createUi();
			
			document.addEventListener("contextmenu", function(e){
			    e.preventDefault();
			}, false);
			window.onkeydown = ev => this.onkeydown(ev);
			window.onmousedown = (ev) => {
				var evt: MouseEvent = <MouseEvent>this.im.mouse.originalEvent;
				if (evt != null && (ev.buttons & 1))
				{
					var loc: PIXI.Point;
					var global = this.im.mouse.global;
					loc = this.im.mouse.getLocalPosition(this.guistage);
					if (!(new PIXI.Rectangle(0, 0, this.renderer.width, this.renderer.height)).contains(global.x, global.y))
					{
						// ignore mouse clicks outside of the game
						return;
					}
					this.idleTimer = Date.now();
					if (theChat.hasFocus())
					{
						theChat.loseFocus();
						return;
					}
					if (this.targetingAbility !== null)
					{
						theClient.sendability(this.targetingAbility);
						this.targetingAbility = null;
						return;
					}
					// inventory screen
					if (this.inventoryScreen.visible)
					{
						if (loc.x >= 314 && loc.x < 514 && loc.y >= 13 && loc.y < 163)
						{
							var col = Math.floor((loc.x - 314) / 50);
							var row = Math.floor((loc.y - 13) / 50);
							
							if (thePlayer && row * 4 + col < thePlayer.inventory.length)
							{
								var extent = thePlayer.inventory[row * 4 + col];
								if (this.objects[extent].getThing().cls & ThingClass.FOOD)
									theClient.senduse(extent);
								else if (thePlayer.equipment.indexOf(extent) >= 0)
									theClient.senddequip(extent);
								else
									theClient.sendequip(extent);
							}
							return;
						}
						if (this.inventoryScreenDown.getBounds().contains(global.x, global.y))
						{
							this.inventoryScreenPage += 1;
							if (this.inventoryScreenPage > 16)
								this.inventoryScreenPage = 16;
							this.updateInventory();
							return;
						}
						if (this.inventoryScreenUp.getBounds().contains(global.x, global.y))
						{
							this.inventoryScreenPage -= 1;
							if (this.inventoryScreenPage < 0)
								this.inventoryScreenPage = 0;
							this.updateInventory();
							return;
						}
					}
					
					// show/hide inventory button
					if (loc.x >= 5 && loc.x < 30 && loc.y >= 650 && loc.y < 680)
					{
						this.inventoryScreen.visible = !this.inventoryScreen.visible;
						this.updateInventory();
						return;
					}
					// swap weapon area
					if (loc.x >= 25 && loc.x <= 80 && loc.y >= 700 && loc.y <= 750)
					{
						theClient.swapweapon();
						return;
					}
					for (var i = 0; i < this.quickBarImages.length; i++)
					{
						if (this.quickBarImages[i].getBounds().contains(global.x, global.y))
						{
							this.targetingAbility = null;
							switch (QuickBarSlots[i])
							{
								case PlayerAbility.ABILITY_BERSERKER_CHARGE:
								case PlayerAbility.ABILITY_HARPOON:
									this.targetingAbility = QuickBarSlots[i];
									break;
								default:
									theClient.sendability(QuickBarSlots[i]);
									break;
							}
							return;
						}
					}
					
					loc = this.im.mouse.getLocalPosition(this.allstage);
					var objs = this.findObjects(loc).filter((x) => (x.getThing().cls & ThingClass.PICKUP) != 0 && x.visible);
					if (objs.length > 0)
					{
						var closest = null;
						var closest2 = Infinity;
						for (var i = 0; i < objs.length; i++)
						{
							var o = objs[i];
							var dist = Math.pow(loc.y - o.y, 2) + Math.pow(loc.x - o.x, 2);
							if (dist < closest2)
							{
								closest = o;
								closest2 = dist;
							}
						}
						
						if (Math.pow(thePlayer.object.x - closest.x, 2) + Math.pow(thePlayer.object.y - closest.y, 2) < 6000)
						{
							theClient.sendpickup(closest.extent);
							return;
						}
					}
					
					theClient.attack();
				}
			};
			window.setTimeout(() => this.tick(), 33);
		}
		onkeydown(ev: KeyboardEvent)
		{
			this.idleTimer = Date.now();
			if (theChat.hasFocus())
			{
				switch (ev.keyCode)
				{
				case 0x1B: // ESC
					theChat.clearInput();
					theChat.loseFocus();
					break;
				case 0x0A: // ENTER
				case 0x0D:
					if (theChat.getInput() != '')
						theClient.sendtext(theChat.getInput());
					theChat.clearInput();
					theChat.loseFocus();
					break;
				default:
					break;
				}
				return;
			}
			switch (ev.keyCode)
			{
				case 0x0A: // ENTER
				case 0x0D:
					theChat.takeFocus();
					break;
				case 0x1B: // ESC
					this.targetingAbility = null;
					this.inventoryScreen.visible = false;
					this.updateInventory();
					break;
				case 0x41: // A
					theClient.sendability(QuickBarSlots[0]);
					break;
				case 0x53: // S
					theClient.sendability(QuickBarSlots[1]);
					break;
				case 0x44: // D
					theClient.sendability(QuickBarSlots[2]);
					break;
				case 0x46: // F
					theClient.sendability(QuickBarSlots[3]);
					break;
				case 0x47: // G
					theClient.sendability(QuickBarSlots[4]);
					break;
				case 0x49: // I
					this.inventoryScreen.visible = !this.inventoryScreen.visible;
					this.updateInventory();
					break;
				case 0x56: // V
					theClient.swapweapon();
					break;
				case 0x58: // X
					thePlayer.useHealth();
					break;
				case 0x5A: // Z
					thePlayer.usePoison();
					break;
				case 0x20: // Space
					theClient.jump();
					break;
			}
		}
		mouseover()
		{
			this.hideTooltip();
				
			// handle mouse over in UI
			var global = this.im.mouse.global;
			var loc = this.im.mouse.getLocalPosition(this.guistage);
			if (loc.x >= 25 && loc.x <= 80 && loc.y >= 700 && loc.y <= 750)
			{
				// weapon area
				if (!thePlayer) return;
				var extent = thePlayer.equipment.filter((extent) => (theWorld.get(extent).getThing().cls & ThingClass.WEAPON) != 0)[0];
				if (!extent) return;
				this.showTooltip(theStrings.messages[theWorld.get(extent).getThing().prettyname][0]);
				return;
			}
			
			for (var i = 0; i < this.quickBarImages.length; i++)
			{
				if (this.quickBarImages[i].getBounds().contains(global.x, global.y))
				{
					var str;
					if (QuickBarSlots[i] == PlayerAbility.ABILITY_BERSERKER_CHARGE)
						str = 'thing.db:BerserkerCharge'
					else if (QuickBarSlots[i] == PlayerAbility.ABILITY_EYE_OF_THE_WOLF)
						str = 'thing.db:EyeOfTheWolf'
					else if (QuickBarSlots[i] == PlayerAbility.ABILITY_HARPOON)
						str = 'thing.db:Harpoon'
					else if (QuickBarSlots[i] == PlayerAbility.ABILITY_TREAD_LIGHTLY)
						str = 'thing.db:TreadLightly'
					else if (QuickBarSlots[i] == PlayerAbility.ABILITY_WARCRY)
						str = 'thing.db:Warcry'
					this.showTooltip(theStrings.messages[str][0]);
					return;
				}
			}
			
			// handle mouse over in game
			var loc = this.im.mouse.getLocalPosition(this.allstage);
			var objs = this.findObjects(loc, 20).filter((x) => (x.getThing().cls & ThingClass.PICKUP) != 0 && x.visible);
			if (objs.length > 0)
			{
				var closest = null;
				var closest2 = Infinity;
				for (var i = 0; i < objs.length; i++)
				{
					var o = objs[i];
					var dist = Math.pow(loc.y - o.y, 2) + Math.pow(loc.x - o.x, 2);
					if (dist < closest2)
					{
						closest = o;
						closest2 = dist;
					}
				}
				
				if (Math.pow(thePlayer.object.x - closest.x, 2) + Math.pow(thePlayer.object.y - closest.y, 2) < 6000)
				{
					this.pickupCircle.x = o.x;
					this.pickupCircle.y = o.y;
					this.pickupCircle.visible = true;
					this.showTooltip(theStrings.messages[o.getThing().prettyname][0]);
					this.renderer.view.style.cursor = 'pointer';
				}
				else
					this.pickupCircle.visible = false;
			}
			else
			{
				this.pickupCircle.visible = false;
			}
		}
		tick(/*timestamp: number*/)
		{			
			if (Date.now() - this.idleTimer >= 5*60*1000) // 3 minute idle timer
			{
				document.location.href = 'idle.html';
				return;
			}
			this.renderer.view.style.cursor = 'default';
			window.setTimeout(() => this.tick(), 33);
			
			this.currentFrame++;
			
			// update server with mouse location and input
			if (thePlayer && thePlayer.object.loaded)
			{
				var evt: MouseEvent = <MouseEvent>this.im.mouse.originalEvent;
				if (evt == null)
				{
					theClient.sendupdate({x: 0, y: 0});
				}
				else
				{
					var loc = this.im.mouse.getLocalPosition(this.allstage);
					if (evt.buttons & 2)
						theClient.move();
					theClient.sendupdate(loc);
				}
			}
			
			// center the stage on the player
			if (thePlayer)
			{
				this.allstage.x = Math.floor(-thePlayer.object.x + this.renderer.width / 2);
				this.allstage.y = Math.floor(-thePlayer.object.y + this.renderer.height / 2); 
				
				if (thePlayer.object.x !== undefined)
					this.infoText.text = 'x: ' + thePlayer.object.x.toString() + ', y: ' + thePlayer.object.y.toString();
				
				// shadows
				var polygons = Visibility.generate(thePlayer.object, this.wallsArray, this.objectsArray);
				this.overlay.clear();
				this.overlay.beginFill(0);
				for (var i = 0; i < polygons.length; i ++)
				{
					var polygon = polygons[i];
					var points = [];
					for (var j = 0; j < polygon.length; j++)
					{
						points.push(polygon[j].x);
						points.push(polygon[j].y);
					}
					this.overlay.drawPolygon(points);
				}
				this.overlay.endFill();
			}
			
			this.effectGraphic.clear();
			for (var i = 0; i < this.effects.length; i++)
			{
				if (!this.effects[i].draw(this.effectGraphic))
				{
					this.effects.splice(i, 1);
					i--;
				}
			}
			
			this.mouseover();
			if (this.targetingAbility !== null)
				this.renderer.view.style.cursor = 'crosshair';
				
			this.updateAbilities();
			this.updateMessages();
			if (thePlayer)
				thePlayer.updateStamina();
				
			var loc = this.im.mouse.getLocalPosition(this.guistage);
			this.toolTipContainer.x = loc.x;
			this.toolTipContainer.y = loc.y + 20;
			
			// render at around 30 fps
			this.renderer.render(this.guistage);
		}
	}
}