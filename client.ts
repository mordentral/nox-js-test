module WSClient
{
	function dumpbuffer(data: ArrayBuffer) : string {
		var _data : Uint8Array = new Uint8Array(data);
		var s : string = '';
		
		for (var i = 0; i < _data.length; i++)
		{
			var hex : string = _data[i].toString(16);
			if (hex.length < 2)
				hex = "0"+hex;
			s += hex.toUpperCase() + " ";
		}
		
		return s;
	}
	
	export class WSClient
	{
		game: Game.Client;
		debug: boolean;
		id: number;
		ackid: number;
		synid: number;
		importantLastTime: number;
		importantSeq: number;
		importantWindow: {[seq: number]: Uint8Array};
		timestamp: number;
		xorkey: number;
		ws: WebSocket;
		playerCls: number;
		playerName: string;
		constructor(url: string, game: Game.Client, name: string, playerCls = Game.PlayerClass.WARRIOR) {
			this.debug = false;
			this.ackid = 0;
			this.synid = 1;
			this.importantLastTime = 0;
			this.importantSeq = 0;
			this.importantWindow = {};
			this.timestamp = 0;
			this.xorkey = null;
			this.playerCls = playerCls;
			this.playerName = name;
			
			this.game = game;
			this.ws = new WebSocket(url, ["game-protocol"]);
			this.ws.binaryType = "arraybuffer";
			this.ws.onopen = ev => this.onopen(ev);
		}
		onopen(ev: Event) {
			this.ws.onmessage = ev => this.onconnectmessage(ev);
			this.ws.send(new Uint8Array([
				0x00,0x00,0x0E,0x00,0x5A,0x00,0x42,0x00,0x6F,0x00,0x74,0x00,0x00,0x00,0x00,0x1A,0x00,0x00
	            ,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xB0,0x48,0x69,0x00,0x00
	            ,0x00,0x00,0x00,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x5A,0x01,0x83,0x01,0xB0
	            ,0x90,0x69,0x00,0x00,0x00,0x00,0x0A,0x34,0x34,0x37,0x35,0x33,0x33,0x30,0x31,0x37
	            ,0x31,0x31,0x36,0x38,0x30,0x32,0x35,0x33,0x35,0x32,0x30,0x38,0x39,0x00,0x9A
	            ,0x03,0x01,0x00,0x00,0x00,0x00,0x00,0x7A,0x6F,0x61,0x65,0x64,0x6B,0x6b,0x6b,0x50
	            ,0xB4,0x00,0x00
			]));
		}
		onconnectmessage(ev: MessageEvent) {
			var _data : ArrayBuffer = ev.data;
			var data : Uint8Array = new Uint8Array(_data);
			
			console.log("RX: " + dumpbuffer(_data));
			
			if (data.length != 3 || data[2] != 0x14)
			{
				window.alert("Connect failed!");
				return;
			}
			
			this.ws.onmessage = ev => this.onmessage(ev); 
			this.ws.send(new Uint8Array([0xFF, 0x00, 0x00]));
		}
		onmessage(ev: MessageEvent) {
			var _data : ArrayBuffer = ev.data;
			var data : Uint8Array = new Uint8Array(_data);
			
			if (this.xorkey !== null)
			{
				for (var i = 0; i < data.length; i++)
					data[i] ^= this.xorkey;
			}
			
			if (data.length < 3)
			{
				console.log("Error: packet too short");
				return;
			}
			
			this.timestamp++;
			
			if (this.debug)
			{
				console.log("RX: " + dumpbuffer(_data));
			}
			
			if (data[0] & 0x80)
			{
				if (this.ackid != data[1])
					return;
				this.ackid++;
			}
			else
			{
				if (this.synid < data[1])
					this.synid = data[1];
			}
			
			// "fix" for delayed ACKs
			if (this.id)
				this.send(new Uint8Array([]));			
			
			var i = 2;
			while (i < data.length)
			{
				var opc : string = data[i].toString(16).toUpperCase();
				if (opc.length < 2)
					opc = "0" + opc;
				var handler : (data: Uint8Array) => number = this["onpacket" + opc];
				if (handler)
				{
					i += 1;
					i += handler.apply(this, [data.subarray(i)]);
				}
				else
				{
					console.log("RX: " + dumpbuffer(_data));
					console.log("Error: missing handler for " + opc + " at " + i.toString(10));	
					break;
				}
			}
			
			if (!this.importantWindow[this.importantSeq] && !_.isEmpty(this.importantWindow))
			{
				// check for timeout
				if (Date.now() - this.importantLastTime > 10000)
				{
					// move seq number forward until we hit something
					while (!this.importantWindow[this.importantSeq])
						this.importantSeq++;
				}
			}
			// process important messages in-order
			for (; this.importantWindow[this.importantSeq]; this.importantSeq++)
			{
				data = this.importantWindow[this.importantSeq];
				for (var i = 0; i < data.length;)
				{
					var opc : string = data[i].toString(16).toUpperCase();
					if (opc.length < 2)
						opc = "0" + opc;
					var handler : (data: Uint8Array) => number = this["onpacket" + opc];
					if (handler)
					{
						i += 1;
						i += handler.apply(this, [data.subarray(i)]);
					}
					else
					{
						console.log("RX: " + dumpbuffer(new Uint8Array(data.subarray(3, 3 + length)).buffer));
						console.log("Error: missing handler for " + opc + " at " + i.toString(10));	
						break;
					}
				}
				delete this.importantWindow[this.importantSeq];
				this.importantLastTime = Date.now();
			}
		}
		send(data: Uint8Array, reliable = false, outofband = false, encrypt = true) {
			if (!outofband)
			{
				var _data = new Uint8Array(data.byteLength + 2);
				_data.set(data, 2);
				data = _data;
				
				if (reliable)
				{
					data[0] = 0x80 | this.id;
					data[1] = this.synid;
					this.synid++;
				}
				else
				{
					data[0] = 0x00 | this.id;
					data[1] = this.ackid;
				}				
			}
			if (this.debug)
				console.log("TX: " + dumpbuffer(data.buffer));
			if (encrypt)
			{
				for (var i = 0; i < data.length; i++)
					data[i] ^= this.xorkey;
			}

			this.ws.send(data);
		}
		onpacket04(data: Uint8Array) : number {
			var buf = [0x1F, 0x01, 0x20];
			var i: number;
			for (i = 0; i < 8 && i < this.playerName.length; i++)
			{
				buf.push(this.playerName.charCodeAt(i) & 0xFF);
				buf.push(this.playerName.charCodeAt(i) >> 8);
			}
			for (; i < 9; i++)
			{
				buf.push(0);
				buf.push(0);
			}
			
			this.send(new Uint8Array(buf.concat(0x74, 0x00, 0x61, 0x00, 0x6C, 0x00, 0x6C, 0x00, 0x6D, 
              0x00, 0x61, 0x00, 0x6E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
              0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
              0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, this.playerCls, 0x00, 0x73, 0x4D, 0x22, 0xDA, 0x9A, 0x6E, 0xDA, 
              0x9A, 0x6E, 0xDA, 0x9A, 0x6E, 0xDA, 0x9A, 0x6E, 0x0C, 0x07, 0x13, 0x17, 0x06, 0x00, 0x00, 0x00, 
              0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 
              0x00, 0x71, 0x34, 0x4C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD0, 0xD4, 0x48, 
              0x00, 0x01, 0x00, 0x00, 0x00, 0x7A, 0x6F, 0x61, 0x65, 0x64, 0x6B, 0x00, 0x00, 0x00, 0x00, 0x00, 
              0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01)), true);
			this.send(new Uint8Array([0xAD]), true);
			this.onpacket04 = null;
			return 0;
		}
		onpacket1F(data: Uint8Array) : number {
			this.id = data[2];
			this.xorkey = data[6];
			
			console.log("id: ", this.id);
			console.log("xor: ", this.xorkey);
			
			this.send(new Uint8Array([0xFF, 0x00, 0x00]), false, true);
			this.onpacket1F = null;
			return 7;
		}
		onpacket27(data: Uint8Array) : number {
			// MSG_TIMESTAMP
			var partialts = data[0] | (data[1] << 8);
			var overflow = ((this.timestamp & 0xFFFF) >= 0xC000) && (partialts < 0x4000);
			this.timestamp = (this.timestamp & 0xFFFF0000) | partialts;
			if (overflow)
				this.timestamp += 0x10000;
			return 2;
		}
		onpacket28(data: Uint8Array) : number {  
			// MSG_FULL_TIMESTAMP
			var timestamp = data[0] | (data[1] << 8) | (data[2] << 16) | (data[3] << 24);
			this.timestamp = timestamp;
			return 4;
		}
		onpacket2B(data: Uint8Array) : number {
			// MSG_USE_MAP
			var name: string = '';
			for (var i = 0; ; i++)
			{
				var ch = data[i];
				if (ch == 0)
					break;
				name += String.fromCharCode(ch);
			}
			
			this.send(new Uint8Array([0xC0]), true);
			
			this.game.onnewmap(name);
			return 40;
		}
		onpacket2C(data: Uint8Array) : number {
			// MSG_JOIN_DATA
			var extent = data[0] | (data[1] << 8);
			console.log(dumpbuffer(data));
			this.game.onjoindata(extent);
			return 6;
		}
		onpacket2D(data: Uint8Array) : number {
			// MSG_NEW_PLAYER
			var extent = data[0] | (data[1] << 8);
			var name: string = '';
			for (var i = 2; ; i += 2)
			{
				var ch = data[i + 0] | (data[i + 1] << 8);
				if (ch == 0)
					break;
				name += String.fromCharCode(ch);
			}
			var wolname: string = '';
			for (var i = 118; ; i++)
			{
				var ch = data[i];
				if (ch == 0)
					break;
				wolname += String.fromCharCode(ch);
			}
			var cls = data[68];
			var respawnArmor = data[103] | (data[104] << 8) | (data[105] << 16) | (data[106] << 24);
			var respawnWeapon = data[107] | (data[108] << 8) | (data[109] << 16) | (data[110] << 24);
			this.game.onnewplayer(extent, cls, name, wolname, respawnArmor, respawnWeapon);
			console.log(dumpbuffer(data));
			return 128;
		}
		onpacket2E(data: Uint8Array) : number {
			// MSG_PLAYER_QUIT
			var id = data[0] | (data[1] << 8);
			this.game.onplayerquit(id);
			return 2;
		}
		onpacket2F(data: Uint8Array) : number {
			return 8;
		}
		onpacket31(data: Uint8Array) : number {
			// MSG_DESTROY_OBJECT
			var id = data[0] | (data[1] << 8);
			this.game.onobjectdestroy(id);
			return 2;
		}
		onpacket32(data: Uint8Array) : number {
			// MSG_OBJECT_OUT_OF_SIGHT
			var id = data[0] | (data[1] << 8);
			this.game.onobjectdestroy(id);
			return 2;
		}
		onpacket33(data: Uint8Array) : number {
			// MSG_OBJECT_IN_SHADOWS
			var id = data[0] | (data[1] << 8);
			this.game.onobjectdestroy(id);
			return 2;
		}
		onpacket34(data: Uint8Array) : number {
			return 2;
		}
		onpacket35(data: Uint8Array) : number {
			return 2;
		}
		onpacket38(data: Uint8Array) : number {
			// MSG_DISABLE_OBJECT
			return 2;
		}
		onpacket39(data: Uint8Array) : number {
			return 3;
		}
		onpacket3A(data: Uint8Array) : number {
			// MSG_DESTROY_WALL
			return 2;
		}
		onpacket3B(data: Uint8Array) : number {
			// MSG_OPEN_WALL
			var id = data[0] | (data[1] << 8);
			this.game.onwallopen(id);
			return 2;
		}
		onpacket3C(data: Uint8Array) : number {
			// MSG_CLOSE_WALL
			var id = data[0] | (data[1] << 8);
			this.game.onwallclose(id);
			return 2;
		}
		onpacket3D(data: Uint8Array) : number {
			return 5;
		}
		onpacket42(data: Uint8Array) : number {
			// MSG_REPORT_PLAYER_HEALTH_DELTA
			var extent = data[0] | (data[1] << 8);
			var delta = data[2] | (data[3] << 8);
			if (delta & 0x8000) // negative
				delta = -((delta ^ 0xFFFF) + 1);
			this.game.onhealthdelta(extent, delta);
			return 4;
		}
		onpacket43(data: Uint8Array) : number {
			var current = data[0] | (data[1] << 8);			
			this.game.onhealth(current);
			return 2;
		}
		onpacket44(data: Uint8Array) : number {
			return 6;
		}
		onpacket45(data: Uint8Array) : number {
			return 4;
		}
		onpacket47(data: Uint8Array) : number {
			// MSG_REPORT_STAMINA
			this.game.onstamina(data[0]);
			return 1;
		}
		onpacket48(data: Uint8Array) : number {
			// MSG_REPORT_STATS
			return 13;
		}
		onpacket49(data: Uint8Array) : number {
			// MSG_REPORT_ARMOR_VALUE
			return 4;
		}
		onpacket4B(data: Uint8Array) : number {
			var id = data[0] | (data[1] << 8);
			var type = data[2] | (data[3] << 8);
			this.game.onpickup(id, type);
			return 4;
		}
		onpacket4C(data: Uint8Array) : number {
			var id = data[0] | (data[1] << 8);
			var type = data[2] | (data[3] << 8);
			this.game.onpickup(id, type);
			return 8;
		}
		onpacket4D(data: Uint8Array) : number {
			var id = data[0] | (data[1] << 8);
			var type = data[2] | (data[3] << 8);
			this.game.ondrop(id, type);
			return 4;
		}
		onpacket4E(data: Uint8Array) : number {
			return 10;
		}
		onpacket4F(data: Uint8Array) : number {
			// MSG_REPORT_MUNDANE_ARMOR_EQUIP
			var id = data[0] | (data[1] << 8);
			var bitmask = data[2] | (data[3] << 8) | (data[4] << 16) | (data[5] << 24);
			this.game.onarmor(id, bitmask, true);
			return 6;
		}
		onpacket50(data: Uint8Array) : number {
			// MSG_REPORT_MUNDANE_WEAPON_EQUIP
			var id = data[0] | (data[1] << 8);
			var bitmask = data[2] | (data[3] << 8) | (data[4] << 16) | (data[5] << 24);
			this.game.onweapon(id, bitmask, true);
			return 6;
		}
		onpacket51(data: Uint8Array) : number {
			// MSG_REPORT_MODIFIABLE_WEAPON_EQUIP
			var id = data[0] | (data[1] << 8);
			var bitmask = data[2] | (data[3] << 8) | (data[4] << 16) | (data[5] << 24);
			this.game.onweapon(id, bitmask, true);
			return 10;
		}
		onpacket52(data: Uint8Array) : number {
			// MSG_REPORT_MODIFIABLE_ARMOR_EQUIP
			var id = data[0] | (data[1] << 8);
			var bitmask = data[2] | (data[3] << 8) | (data[4] << 16) | (data[5] << 24);
			this.game.onarmor(id, bitmask, true);
			return 10;
		}
		onpacket53(data: Uint8Array) : number {
			// MSG_REPORT_ARMOR_DEQUIP
			var id = data[0] | (data[1] << 8);
			var bitmask = data[2] | (data[3] << 8) | (data[4] << 16) | (data[5] << 24);
			this.game.onarmor(id, bitmask, false);
			return 6;
		}
		onpacket54(data: Uint8Array) : number {
			// MSG_REPORT_WEAPON_DEQUIP
			var id = data[0] | (data[1] << 8);
			var bitmask = data[2] | (data[3] << 8) | (data[4] << 16) | (data[5] << 24);
			this.game.onweapon(id, bitmask, false);
			return 6;
		}
		onpacket5A(data: Uint8Array) : number {
			// MSG_REPORT_ENCHANTMENT
			var id = data[0] | (data[1] << 8);
			var enchant = data[2] | (data[3] << 8) | (data[4] << 16) | (data[5] << 24);
			this.game.onenchantment(id, enchant);
			return 6;
		}
		onpacket5B(data: Uint8Array) : number {
			return 1;
		}
		onpacket5E(data: Uint8Array) : number {
			// MSG_REPORT_Z_PLUS;
			return 3;
		}
		onpacket60(data: Uint8Array) : number {
			var id = data[0] | (data[1] << 8);
			this.game.onequip(id);
			return 2;
		}
		onpacket61(data: Uint8Array) : number {
			var id = data[0] | (data[1] << 8);
			this.game.ondequip(id);
			return 2;
		}
		onpacket64(data: Uint8Array) : number {
			// MSG_REPORT_CHARGES
			var extent: number = data[0] | (data[1] << 8);
			this.game.oncharges(extent, data[2], data[3]);
			return 4;
		}
		onpacket67(data: Uint8Array) : number {
			// MSG_REPORT_MODIFIER
			return 6;
		}
		onpacket68(data: Uint8Array) : number {
			// MSG_REPORT_STAT_MODIFIER
			return 7;
		}
		onpacket6A(data: Uint8Array) : number {
			var extent: number = data[0] | (data[1] << 8);
			this.game.onsetobs(extent, data[2] == 1);
			console.log("Set observation", extent, data[2]);
			return 6;
		}
		onpacket6B(data: Uint8Array) : number {
			return 6;
		}
		onpacket81(data: Uint8Array) : number {
			return 4;
		}
		onpacket82(data: Uint8Array) : number {
			return 4;
		}
		onpacket83(data: Uint8Array) : number {
			return 4;
		}
		onpacket84(data: Uint8Array) : number {
			return 4;
		}
		onpacket85(data: Uint8Array) : number {
			return 4;
		}
		onpacket86(data: Uint8Array) : number {
			return 4;
		}
		onpacket87(data: Uint8Array) : number {
			return 4;
		}
		onpacket88(data: Uint8Array) : number {
			return 4;
		}
		onpacket89(data: Uint8Array) : number {
			return 4;
		}
		onpacket8A(data: Uint8Array) : number {
			return 4;
		}
		onpacket8B(data: Uint8Array) : number {
			return 4;
		}
		onpacket8C(data: Uint8Array) : number {
			return 8;
		}
		onpacket8D(data: Uint8Array) : number {
			return 8;
		}
		onpacket8E(data: Uint8Array) : number {
			return 8;
		}
		onpacket8F(data: Uint8Array) : number {
			return 8;
		}
		onpacket90(data: Uint8Array) : number {
			return 8;
		}
		onpacket91(data: Uint8Array) : number {
			return 8;
		}
		onpacket93(data: Uint8Array) : number {
			return 5;
		}
		onpacket94(data: Uint8Array) : number {
			var x1: number = data[0] | (data[1] << 8);
			var y1: number = data[2] | (data[3] << 8);
			var x2: number = data[4] | (data[5] << 8);
			var y2: number = data[6] | (data[7] << 8);
			this.game.onfxdeathray(x1, y1, x2, y2);
			return 8;
		}
		onpacket95(data: Uint8Array) : number {
			return 8;
		}
		onpacket96(data: Uint8Array) : number {
			return 4;
		}
		onpacket97(data: Uint8Array) : number {
			return 1;
		}
		onpacket9E(data: Uint8Array) : number {
			// duration spell
			var type: number = data[0] | (data[1] << 8);
			var extent1: number = data[2] | (data[3] << 8);
			var extent2: number = data[4] | (data[5] << 8);
			this.game.onfxduration(type, extent1, extent2);
			return 6;
		}
		onpacketA4(data: Uint8Array) : number {
			var i : number = 0;
			var prevloc : number[] = null;
			
			while (true)
			{
				if (data[i] == 0x00)
				{
					if (prevloc == null)
						break;
					prevloc = null;
					i++;
					continue;
				}
				if (data[i] != 0xFF)
				{
					console.log("Unknown byte in A4: " + dumpbuffer(data.buffer.slice(i)));
					break;
				}
				i++;
				
				var extent = data[i] | (data[i+1] << 8);
				i += 2;
				var type = data[i] | (data[i+1] << 8);
				i += 2;
				
				var loc : number[] = Array<number>(2);
				if (prevloc === null)
				{
					loc[0] = data[i] | (data[i+1] << 8);
					i += 2;
					loc[1] = data[i] | (data[i+1] << 8);
					i += 2;
				}
				else
				{
					loc[0] = prevloc[0];
					if (data[i] & 0x80)
						loc[0] -= (data[i] ^ 0xFF) + 1;
					else
						loc[0] += data[i];
					i++;
					loc[1] = prevloc[1];
					if (data[i] & 0x80)
						loc[1] -= (data[i] ^ 0xFF) + 1;
					else
						loc[1] += data[i];
					i++;
				}
				prevloc = loc;
				
				this.game.onupdate(extent, type, loc[0], loc[1], data.subarray(i));
				
				//console.log("Extent: " + extent.toString(10) + ", type: " + type.toString(16));
				// FIXME this needs to be expanded a bit
				if (type == 0x000 || type == 0x2C9)
				{
					// NewPlayer
					var t = data[i];
					i++;
					if (t & 0x80)
						i++; // frame used for Slave animation
					i++;
				}
				else if (type == 0x20E)
				{
					// HarpoonBolt
					console.log(data[i], data[i+1]);
					i++;
					i++;
				}
				else if (type == 0x2B5 || type == 0x2B7)
				{
					// Fireball
					console.log(data[i]); // orientation for animation
					i++;
				}
				else if (type == 0x499)
				{
					// FanChakramInMotion
					console.log(data[i]);
					i++;
				}
				else if (type == 0x49B)
				{
					// FanChakramInMotion
					console.log(data[i]);
					i++;
				}
			}

			return i + 2;
		}
		onpacketA6(data: Uint8Array) : number {
			// MSG_AUDIO_EVENT
			return 3;
		}
		onpacketA7(data: Uint8Array) : number {
			// MSG_AUDIO_PLAYER_EVENT
			return 3;
		}
		onpacketA8(data: Uint8Array) : number {
			// MSG_TEXT_MESSAGE
			var extent: number = data[0] | (data[1] << 8);
			var flags: number = data[2];
			var x: number = data[3] | (data[4] << 8);
			var y: number = data[5] | (data[6] << 8);
			var length: number = data[7] | (data[8] << 8);
			console.log(data[2], data[9]);
			var msg: string = '';
			if (flags & 4) // unicode
			{
				for (var i = 10; i < 10 + length * 2; i += 2)
				{
					var ch = data[i + 0] | (data[i + 1] << 8);
					if (ch == 0)
						break;
					msg += String.fromCharCode(ch);
				}
				this.game.onchat(extent, msg, x, y);
				return 10 + length * 2;
			}
			else if (flags & 2) // ascii
			{
				for (var i = 10; i < 10 + length; i++)
				{
					var ch = data[i];
					if (ch == 0)
						break;
					msg += String.fromCharCode(ch);
				}
				this.game.onchat(extent, msg, x, y);
				return 10 + length;
			}
		}
		onpacketA9(data: Uint8Array) : number {
			// MSG_INFORM
			switch (data[0])
			{
			case 0:
				return 5;
			case 1:
				return 5;
			case 2:
				return 5;
			case 3:
				return 5;
			case 4:
				return 5;
			case 5:
				return 9;
			case 6:
				return 9;
			case 7:
				return 9;
			case 8:
				return 5;
			case 9:
				return 9;
			case 10:
				return 9;
			case 11:
				return 9;
			case 12:
				return 5;
			case 13:
				return 5;
			case 14:
				var attacker1: number = data[1] | (data[2] << 8);
				var attacker2: number = data[3] | (data[4] << 8);
				var victim: number = data[5] | (data[6] << 8);
				this.game.oninformkill(attacker1, attacker2, victim);
				return 10;
			case 15:
				var str: string = '';
				for (var i = 2; ; i++)
				{
					var ch = data[i];
					if (ch == 0)
						break;
					str += String.fromCharCode(ch);
				}
				this.game.oninformmsg(str);
				return i + 1;
			default:
				return 1; 
			}
		}
		onpacketAA(data: Uint8Array) : number {
			// MSG_IMPORTANT
			this.sendackimportant();
			return 0;
		}
		onpacketAE(data: Uint8Array) : number {
			// MSG_OUTGOING_CLIENT
			return 2;
		}
		onpacketAF(data: Uint8Array) : number {
			// MSG_GAME_SETTINGS
			return 19;
		}
		onpacketB0(data: Uint8Array) : number {
			// MSG_GAME_SETTINGS_2
			return 48;
		}
		onpacketB1(data: Uint8Array) : number {
			// MSG_UPDATE_GUI_GAME_SETTINGS
			return 59;
		}
		onpacketB2(data: Uint8Array) : number {
			var extent: number = data[0] | (data[1] << 8);
			this.game.ondoorangle(extent, data[2]);
			return 3;
		}
		onpacketB3(data: Uint8Array) : number {
			var extent: number = data[0] | (data[1] << 8);
			this.game.onobeliskcharge(extent, data[2]);
			return 3;
		}
		onpacketB4(data: Uint8Array) : number {
			return 3;
		}
		onpacketB5(data: Uint8Array) : number {
			return 13;
		}
		onpacketC3(data: Uint8Array) : number {
			console.log("PLAYER_OBJ: " + dumpbuffer(new Uint8Array(data.subarray(0, 11)).buffer));
			return 11;
		}
		onpacketC4(data: Uint8Array) : number {
			switch (data[0])
			{
			case 1:
				return 9;
			case 2:
				return 5;
			case 3:
				return 9;
			case 9:
				return 1;
			default:
				console.log("Unknown TEAM_MSG packet: " + data[0].toString(10));
				return 1;
			}
		}
		onpacketC7(data: Uint8Array) : number {
			// MSG_SERVER_QUIT
			this.send(new Uint8Array([0xC8]));
			this.ws.onmessage = ev => { return; };
			this.ws.close();
			return 0;
		}
		onpacketCC(data: Uint8Array) : number {
			// MSG_SEQ_IMPORTANT
			var seq = data[0] | (data[1] << 8);
			if (seq < this.importantSeq)
			{
				// ignore since we already have this message
				console.log("Dropped important", seq);
				return 3 + length;
			}
				
			var length = data[2];
			this.importantWindow[seq] = new Uint8Array(data.buffer, data.byteOffset + 3, length);
			return 3 + length;
		}
		onpacketCD(data: Uint8Array) : number {
			return 2;
		}
		onpacketCE(data: Uint8Array) : number {
			// MSG_REPORT_ABILITY_STATE
			var ability: number = data[0];
			var available: boolean = data[1] != 0;
			this.game.onabilitystate(ability, available);
			return 2;
		}
		onpacketCF(data: Uint8Array) : number {
			// MSG_REPORT_ACTIVE_ABILITIES
			var ability: number = data[0];
			var active: boolean = data[1] != 0;
			return 2;
		}
		onpacketD2(data: Uint8Array) : number {
			var extent = data[0] | (data[1] << 8);
			var type = data[2] | (data[3] << 8);
			return 6;
		}
		onpacketD3(data: Uint8Array) : number {
			return 12;
		}
		onpacketD7(data: Uint8Array) : number {
			// MSG_REPORT_ALL_LATENCY
			return 4;
		}
		onpacketDD(data: Uint8Array) : number {
			// MSG_REPORT_TOTAL_HEALTH
			var extent = data[0] | (data[1] << 8);
			var current = data[2] | (data[3] << 8);
			var total = data[4] | (data[5] << 8);
			this.game.onhealth(current, total);
			return 6;
		}
		onpacketDE(data: Uint8Array) : number {
			// MSG_REPORT_TOTAL_MANA
			var extent = data[0] | (data[1] << 8);
			var current = data[1] | (data[2] << 8);
			var total = data[3] | (data[4] << 8);
			this.game.onhealth(current, total);
			return 6;
		}
		onpacketDF(data: Uint8Array) : number {
			// MSG_REPORT_SPELL_STAT
			return 5;
		}
		onpacketE4(data: Uint8Array) : number {
			// MSG_FADE_BEGIN
			return 2;
		}
		onpacketE8(data: Uint8Array) : number {
			// MSG_PLAYER_DIED
			var extent = data[0] | (data[1] << 8);
			this.game.onplayerdied(extent);
			return 2;
		}
		onpacketE9(data: Uint8Array) : number {
			return 8;
		}
		onpacketEA(data: Uint8Array) : number {
			// MSG_FORGET_DRAWABLES
			var timestamp = data[0] | (data[1] << 8) | (data[2] << 16) | (data[3] << 24);
			this.game.onforgetdrawables(timestamp);
			return 4;
		}
		onpacketEB(data: Uint8Array) : number {
			// MSG_RESET_ABILITIES
			return 1;
		}
		onpacketEC(data: Uint8Array) : number {
			// MSG_RATE_CHANGE
			return 1;
		}
		onpacketEF(data: Uint8Array) : number {
			// MSG_STAT_MULTIPLIERS
			return 16;
		}
		sendackimportant() : void {
			this.send(new Uint8Array([0xAB,
				this.timestamp & 0xff, (this.timestamp >> 8) & 0xff,
				(this.timestamp >> 16) & 0xff, (this.timestamp >> 24) & 0xff]));
		}
	}
}