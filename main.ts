/// <reference path="game.ts" />
/// <reference path="loader.ts" />
/// <reference path="pixi.d.ts" />

function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
    console.log('Query variable %s not found', variable);
}

var renderer = new PIXI.CanvasRenderer(1024, 768);

PIXI.ticker.shared.autoStart = false;
PIXI.ticker.shared.stop();

document.getElementById('game').appendChild(renderer.view);

var server = getQueryVariable('server');
if (!server)
	server = 'america';

var servers = {
	europe: "ws://81.4.107.152:7681",
	america: "ws://198.46.193.17:7681"
};

Loader.waitForAppCache(renderer, () => {
	Game.main(servers[server], renderer, <HTMLDivElement>document.getElementById('chatlog'), <HTMLInputElement>document.getElementById('chatinput'));
});