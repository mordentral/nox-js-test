/// <reference path="game.ts" />

// based off of http://www.redblobgames.com/articles/visibility/Visibility.hx
module Visibility
{
	class Point
	{
		x: number;
		y: number;
		constructor(x: number, y: number)
		{
			this.x = x;
			this.y = y;
		}
		distance(p2: Point)
		{
			var dx = p2.x - this.x;
			var dy = p2.y - this.y;
			return dx*dx + dy*dy;
		}
	}
	
	class EndPoint extends Point
	{
		begin: boolean;
		segment: Segment;
		angle: number;
	}
	
	class Segment
	{
		p1: EndPoint;
		p2: EndPoint;
		d: number;
		next: Segment;
		prev: Segment;
		drawable: Game.IDrawable;
		constructor()
		{
			this.next = null;
			this.prev = null;
		}
	}

	export var segments: Segment[];
	var endpoints: EndPoint[];
	var objects: Game.IDrawable[];
	var center: Point;
	var output: Point[][];
	var currentPolygon: Point[];
	var currentFirstAngle: number;
	
	function addSegment(drawable: Game.IDrawable, x1: number, y1: number, x2: number, y2: number)
	{
		var p1 = new EndPoint(x1, y1);
		var p2 = new EndPoint(x2, y2);
		var segment = new Segment();
		segment.p1 = p1;
		segment.p2 = p2;
		segment.d = 0;
		segment.drawable = drawable;
		p1.segment = segment;
		p2.segment = segment;
		
		segments.push(segment);
		endpoints.push(p1);
		endpoints.push(p2);
	}
	
	function calculateCenter()
	{
		for (var i = 0; i < segments.length; i++)
		{
			var s = segments[i];
			var dx = 0.5 * (s.p1.x + s.p2.x) - center.x;
			var dy = 0.5 * (s.p1.y + s.p2.y) - center.y;
			s.d = dx*dx + dy*dy;
			
			s.p1.angle = Math.atan2(s.p1.y - center.y, s.p1.x - center.x);
			s.p2.angle = Math.atan2(s.p2.y - center.y, s.p2.x - center.x);
			
			var dAngle = s.p2.angle - s.p1.angle;
			if (dAngle <= -Math.PI) { dAngle += 2*Math.PI };
			if (dAngle > Math.PI) { dAngle -= 2*Math.PI };
			s.p1.begin = (dAngle > 0.0);
			s.p2.begin = !s.p1.begin;
		}
	}
	
	function sortByAngle(a: EndPoint, b: EndPoint)
	{
		if (a.angle > b.angle) return 1;
		if (b.angle > a.angle) return -1;
		if (!a.begin && b.begin) return -1;
		if (!b.begin && a.begin) return 1;
		return 0;	
	}
	
	function sweep()
	{
		if (endpoints.length == 0)
			return;
			
		endpoints.sort(sortByAngle);
		if (!endpoints[0].begin)
		{
			endpoints[0].angle += Math.PI*2;
			endpoints.push(endpoints.splice(0, 1)[0]);
		}
		
		currentPolygon = null;
		var beginAngle = 0.0;
		var listHead: Segment = null;
		var objectHead = 0;
		for (var pass = 0; pass < 2; pass++)
		{
			for (var i = 0; i < endpoints.length; i++)
			{
				var p = endpoints[i];
				var oldHead = listHead;
				if (p.segment.p1.angle == p.segment.p2.angle)
					continue;
				if (pass == 1 && objectHead < objects.length)
				{
					var o = objects[objectHead];
					while (o.angle < p.angle)
					{
						o.visible = oldHead == null || oldHead.d >= o.d;
						
						objectHead++;
						if (objectHead >= objects.length) break;
						o = objects[objectHead]
					}
				}
				if (p.begin)
				{
					var s = oldHead;
					if (s == null || s.d >= p.segment.d)
					{
						p.segment.next = listHead;
						listHead = p.segment;
						if (p.segment.next)
							p.segment.next.prev = p.segment;
					}
					else
					{
						while (s.next != null && s.next.d < p.segment.d)
							s = s.next;
						p.segment.next = s.next
						s.next = p.segment;
						p.segment.prev = s;
						if (p.segment.next)
							p.segment.next.prev = p.segment;
					}
					
					//open.splice(_.sortedIndex(open, p.segment, (s) => { return s.d; }), 0, p.segment);
				}
				else
				{
					//open.splice(open.indexOf(p.segment), 1);
					if (p.segment == listHead)
						listHead = p.segment.next;
					if (p.segment.next)
						p.segment.next.prev = p.segment.prev;
					if (p.segment.prev)
						p.segment.prev.next = p.segment.next;
				}
				
				var head = listHead;
				if (oldHead != head)
				{
					if (pass == 1)
						addPolygon(beginAngle, p.angle, oldHead);
					if (p.angle < beginAngle && currentPolygon)
						currentFirstAngle -= Math.PI*2;
					beginAngle = p.angle;
				}
			}
		}
		
		// XXX is this necessary?
		if (objectHead < objects.length)
		{
			var o = objects[objectHead];
			while (true)
			{
				o.visible = listHead == null || listHead.d >= o.d;
				
				objectHead++;
				if (objectHead >= objects.length) break;
				o = objects[objectHead];
			}
		}
		
		if (listHead)
		{
			addPolygon(beginAngle, endpoints[0].angle, listHead);
			beginAngle = p.angle;
		}
		
		addPolygon(beginAngle, null, null);
	}
	
	// From http://paulbourke.net/geometry/lineline2d/
	function lineIntersection(p1: Point, p2: Point, p3: Point, p4: Point): Point
	{
		var s = ((p4.x - p3.x) * (p1.y - p3.y) - (p4.y - p3.y) * (p1.x - p3.x)) / ((p4.y - p3.y) * (p2.x - p1.x) - (p4.x - p3.x) * (p2.y - p1.y));
		return new Point(p1.x + s * (p2.x - p1.x), p1.y + s * (p2.y - p1.y));
	}
	
	function addPolygon(angle1: number, angle2: number, segment: Segment)
	{
		var p1:Point = center;
        var p2:Point = new Point(center.x + Math.cos(angle1), center.y + Math.sin(angle1));
        var p3:Point = new Point(0.0, 0.0);
        var p4:Point = new Point(0.0, 0.0);
		var p5:Point = new Point(0.0, 0.0);
		var p6:Point = new Point(0.0, 0.0);
		
		if (segment != null)
		{
			if (currentPolygon == null)
			{
				currentPolygon = [];
				currentFirstAngle = angle1;
				output.push(currentPolygon);
			}
			
            // Stop the triangle at the intersecting segment
            p3.x = segment.p1.x;
            p3.y = segment.p1.y;
            p4.x = segment.p2.x;
            p4.y = segment.p2.y;
			
			p5.x = center.x + Math.cos(angle1) * 700;
            p5.y = center.y + Math.sin(angle1) * 700;
            p6.x = center.x + Math.cos(angle2) * 700;
            p6.y = center.y + Math.sin(angle2) * 700;
    
	        var pBegin = lineIntersection(p3, p4, p1, p2);
	
	        p2.x = center.x + Math.cos(angle2);
	        p2.y = center.y + Math.sin(angle2);
	        var pEnd = lineIntersection(p3, p4, p1, p2);

			if (angle2 - angle1 > 0.0001 || (angle2 < 0 && angle1 > 0))
			{
				if (currentPolygon.length == 0 || currentPolygon[currentPolygon.length - 1].distance(pBegin) > 1000)
					currentPolygon.push(pBegin);
				currentPolygon.push(pEnd);
				
				segment.drawable.visible = true;
			}
        }
		else if (angle2 === null || angle2 - angle1 > 0.00001)
		{
			if (currentPolygon)
			{
				if (currentFirstAngle > angle1)
					currentFirstAngle -= Math.PI * 2;
					
	            p6.x = center.x + Math.cos(angle1) * 1000;
	            p6.y = center.y + Math.sin(angle1) * 1000;
				currentPolygon.push(p6);
				
				for (var i = angle1; i > currentFirstAngle; i -= Math.PI / 2)
				{				
					var a = Math.floor(i / (Math.PI / 2)) * (Math.PI / 2);
					currentPolygon.push(new Point(center.x + Math.cos(a) * 1000, center.y + Math.sin(a) * 1000));
				}
				
				p5.x = center.x + Math.cos(currentFirstAngle) * 1000;
	            p5.y = center.y + Math.sin(currentFirstAngle) * 1000;
				currentPolygon.push(p5);
			}
			currentPolygon = null;
		}
	}
	
	export function generate(c: {x: number, y: number}, walls: Game.Wall[], _objects: Game.Object[])
	{
		segments = [];
		endpoints = [];
		output = [];
		objects = [];
		
		center = new Point(1.0 * c.x, 1.0 * c.y);
		for (var key = 0; key < _objects.length; key++)
		{
			var o = _objects[key];
			var x = o.x, y = o.y;
			var dx = c.x - x, dy = c.y - y;
			var dist = dx * dx + dy * dy;
			
			o.d = dist;
			o.visible = false;
			
			// (1024/2)^2 + (768/2)^2 = (630)^2
			if (dist > 422500)
				continue;
			
			if (o.getThing().flags & Game.ThingFlags.SHADOW)
			{
				var dx = Math.cos((o.currentFrame + 20) * Math.PI * 2 / 32) * 33;
				var dy = Math.sin((o.currentFrame + 20) * Math.PI * 2 / 32) * 33;
				addSegment(o, o.x, o.y, o.x + dx, o.y + dy);
			}
			else
			{
				o.d = dist;
				o.angle = Math.atan2(y - center.y, x - center.x);
				objects.push(o);
			}
		}
		
		for (var key = 0; key < walls.length; key++)
		{
			var wall = walls[key];
			var x = wall.x, y = wall.y;
			var dx = c.x - x, dy = c.y - y;
			var dist = dx * dx + dy * dy;
			
			wall.visible = false;
			
			// (1024/2)^2 + (768/2)^2 = (630)^2
			if (dist > 422500)
				continue;
			if (wall.window && dist < 2116)
			{
				wall.d = dist;
				wall.angle = Math.atan2(y - center.y, x - center.x);
				objects.push(wall);
				continue;
			}

			x += 11.5;
			y += 11.5;

			var length = 12;
			switch (wall.facing)
			{
				case Game.WallFacing.DOWN:
					addSegment(wall, x - length, y - length, x, y);
					addSegment(wall, x, y, x + length, y + length);
					break;
				case Game.WallFacing.UP:
					addSegment(wall, x - length, y + length, x, y);
					addSegment(wall, x, y, x + length, y - length);
					break;
				case Game.WallFacing.ARROWDOWN:
					addSegment(wall, x - length, y - length, x, y);
					addSegment(wall, x + length, y - length, x, y);
					break;
				case Game.WallFacing.ARROWUP:
					addSegment(wall, x - length, y + length, x, y);
					addSegment(wall, x + length, y + length, x, y);
					break;
				case Game.WallFacing.ARROWLEFT:
					addSegment(wall, x + length, y + length, x, y);
					addSegment(wall, x + length, y - length, x, y);
					break;
				case Game.WallFacing.ARROWRIGHT:
					addSegment(wall, x - length, y + length, x, y);
					addSegment(wall, x - length, y - length, x, y);
					break;
				case Game.WallFacing.TNORTHEAST:
					addSegment(wall, x - length, y - length, x, y);
					addSegment(wall, x, y, x + length, y + length);
					addSegment(wall, x, y, x + length, y - length);
					break;
				case Game.WallFacing.TNORTHWEST:
					addSegment(wall, x - length, y - length, x, y);
					addSegment(wall, x - length, y + length, x, y);
					addSegment(wall, x, y, x + length, y - length);
					break;
				case Game.WallFacing.TSOUTHEAST:
					addSegment(wall, x, y, x + length, y + length);
					addSegment(wall, x - length, y + length, x, y);
					addSegment(wall, x, y, x + length, y - length);
					break;
				case Game.WallFacing.TSOUTHWEST:
					addSegment(wall, x - length, y - length, x, y);
					addSegment(wall, x, y, x + length, y + length);
					addSegment(wall, x - length, y + length, x, y);
					break;
				case Game.WallFacing.CROSS:
					addSegment(wall, x - length, y - length, x, y);
					addSegment(wall, x, y, x + length, y + length);
					addSegment(wall, x - length, y + length, x, y);
					addSegment(wall, x, y, x + length, y - length);
					break;
			}
		}
		
		
		objects.sort((a, b) => a.angle - b.angle);
		
		calculateCenter();
		sweep();
		
		return output;
	}
}