/// <reference path="pixi.d.ts" />

module Loader
{
	var _ondone: () => void;
	var _renderer: PIXI.CanvasRenderer | PIXI.WebGLRenderer;
	
	function onCached(evt: Event)
	{
		_ondone();
		_ondone = () => {};
	}
	
	function onDownloading(evt: Event)
	{
		showLoadingUi();
	}
	
	function onNoUpdate(evt: Event)
	{
		_ondone();
		_ondone = () => {};
	}
	
	function onProgress(evt: ProgressEvent)
	{
		showLoadingUi(evt.loaded, evt.total)
	}
	
	function onUpdateReady()
	{
		document.location.reload();
	}
	
	export function showLoadingUi(loaded?: number, total?: number)
	{
		var container = new PIXI.Container();
		container.x = 319;
		container.y = 312;
		var bg = PIXI.Sprite.fromImage('assets/images/14589.png');
		var bar = PIXI.Sprite.fromImage('assets/images/14590.png');
		container.addChild(bg);
		
		var loadingText = new PIXI.Text("Loading...", {font: 'bold 20pt Arial', fill: 'white'});
		loadingText.x = 192 - loadingText.width / 2;
		loadingText.y = 25;
		container.addChild(loadingText);
		
		if (loaded != null)
		{
			if (!bar.texture.baseTexture.isLoading)
				bar.texture.frame = new PIXI.Rectangle(0, 0, Math.floor(296 * loaded / total), 30);
			container.addChild(bar);
			bar.x = 45;
			bar.y = 85;
			var text = new PIXI.Text(loaded.toString() + " / " + total.toString(), {font: '12pt Arial', fill: 'white', stroke: 'black', strokeThickness: 1});
			text.x = 192 - text.width / 2;
			text.y = 90;
			container.addChild(text);
		}
		
		if (bg.texture.baseTexture.isLoading)
			bg.texture.baseTexture.addListener('loaded', () => _renderer.render(container));
		if (bar.texture.baseTexture.isLoading && loaded != null)
		{
			bar.texture.baseTexture.addListener('loaded', () => {
				bar.texture.frame = new PIXI.Rectangle(0, 0, Math.floor(296 * loaded / total), 30);
				_renderer.render(container);
			});
		}
			
        _renderer.render(container);
	}
	
	export function waitForAppCache(renderer: PIXI.CanvasRenderer | PIXI.WebGLRenderer, ondone: () => void)
	{
		_ondone = ondone;
		_renderer = renderer;
		
		window.applicationCache.oncached = onCached;
		window.applicationCache.ondownloading = onDownloading;
		window.applicationCache.onnoupdate = onNoUpdate;
		window.applicationCache.onprogress = onProgress;
		window.applicationCache.onupdateready = onUpdateReady;
	}
}